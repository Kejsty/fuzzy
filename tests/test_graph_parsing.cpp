#include "catch.hpp"
#include <iostream>
#include <string>
#include <sys/stat.h>

#include "../src/graph.h"
#include <array>
#include <stdlib.h>
#include <unistd.h>

// Some real outputs of divine graphs
namespace inputs {
const std::string prefix = "examples/graphs/";
const std::string in1 = prefix + "fuzzy_graph0";
const std::string in2 = prefix + "fuzzy_graph1";
const std::string in3 = prefix + "fuzzy_graph2";
const std::string in4 = prefix + "fuzzy_graph3";
const std::string in5 = prefix + "fuzzy_graph4";
const std::string in6 = prefix + "fuzzy_graph5";
const std::string in7 = prefix + "fuzzy_graph6";
std::array<std::string, 7> all = { { in1, in2, in3, in4, in5, in6, in7 } };
} // namespace inputs

TEST_CASE("Build")
{
    for (auto& name : inputs::all) {
        divineGraph graph{ name };
        REQUIRE(graph.is_ok());
    }
}

TEST_CASE("getTraces finishes")
{
    for (auto& name : inputs::all) {
        divineGraph graph{ name };
        REQUIRE(graph.getAllTraces().size() > 0);
    }
}

/**
 * For next test cases: you can generate the graph yoursel on
 * http://www.webgraphviz.com/, and then check that the trie contains all
 * expected sequences of system calls
 */

TEST_CASE("inputs::in1 simple")
{
    divineGraph graph{ inputs::in1 };
    REQUIRE(graph.is_ok());
    auto traces = graph.getAllTraces();
    REQUIRE(traces.size() == 4);

    //check all
    REQUIRE(traces.find("1049602,1049600,1049600,") != traces.end());
    REQUIRE(traces.find("1049602,1049600,1049600,1049600,1049600,1049600,1049600,") != traces.end());
    REQUIRE(traces.find("1050626,1050624,1050624,1050624,1050624,1050624,1050624,") != traces.end());
    REQUIRE(traces.find("1051650,1051648,1051648,1051648,1051648,1051648,1051648,") != traces.end());
}

TEST_CASE("inputs::in5 with some errors and faults")
{
    divineGraph graph{ inputs::in5 };
    REQUIRE(graph.is_ok());
    auto traces = graph.getAllTraces();

    REQUIRE(traces.size() == 33);
    //check some
    REQUIRE(traces.find("1051650,1051648,1051648,1051648,1051648,1051648,1051648,1051648,2100227,3148800,") != traces.end());
    REQUIRE(traces.find("1049602,1049600,1049600,1049600,1049600,1049600,1049600,2100226,3148800,") != traces.end());
    REQUIRE(traces.find("1049602,1049600,1049600,2099254,3147776,") != traces.end());
    REQUIRE(traces.find("2098178,3146752,") != traces.end()); //errors
    REQUIRE(traces.find("2100226,3148800,") != traces.end()); //errors
}

TEST_CASE("inputs::in6 with some errors and faults")
{
    divineGraph graph{ inputs::in6 };
    REQUIRE(graph.is_ok());
    auto traces = graph.getAllTraces();

    REQUIRE(traces.size() == 48);
    //check some
    REQUIRE(traces.find("1049602,1049600,1049600,1049600,2098179,3146752,") != traces.end());
    REQUIRE(traces.find("1051650,1051648,1051648,1051648,1051648,1051648,1051648,1051648,2100227,3148800,") != traces.end());
    REQUIRE(traces.find("2098178,3146752,") != traces.end()); //errors
    REQUIRE(traces.find("2100226,3148800,") != traces.end()); //errors
}

TEST_CASE("size and differentiate with same graph")
{
    divineGraph graph{ inputs::in7 };
    REQUIRE(graph.is_ok());

    auto traces1 = graph.getAllTraces();
    REQUIRE(traces1.size() == 2);
    //check all
    REQUIRE(traces1.find("1048617,3145728,") != traces1.end());
    REQUIRE(traces1.find("1048617,1048618,1048577,1048577,1048577,1048577,1048577,") != traces1.end());
}

TEST_CASE("Longest Path")
{
    /**
   * 32 because the last node (34) does not count, as there is only
   * transition to it, but not node itself defined
   */
    SECTION("Simple graph")
    {
        divineGraph graph{ inputs::in7 };
        REQUIRE(graph.get_max_depth() == 32);
    }

    SECTION("Wide graph")
    {
        divineGraph graph{ inputs::in1 };
        // because divine cuts traces after 33 steps
        REQUIRE(graph.get_max_depth() == 32);
    }

    SECTION("Wide graph2")
    {
        divineGraph graph{ inputs::in2 };
        REQUIRE(graph.get_max_depth() == 11);
    }

    SECTION("Wide graph3")
    {
        divineGraph graph{ inputs::in3 };
        REQUIRE(graph.get_max_depth() == 27);
    }
}

TEST_CASE("Maximal Fanout")
{
    SECTION("Simple graph")
    {
        divineGraph graph{ inputs::in7 };
        REQUIRE(graph.get_max_fanout() == 2);
    }

    SECTION("Wide graph")
    {
        divineGraph graph{ inputs::in1 };
        // because divine cuts traces after 33 steps
        REQUIRE(graph.get_max_fanout() == 4);
    }

    SECTION("Wide graph2")
    {
        divineGraph graph{ inputs::in2 };
        REQUIRE(graph.get_max_fanout() == 3);
    }

    SECTION("Wide graph3")
    {
        divineGraph graph{ inputs::in3 };
        REQUIRE(graph.get_max_fanout() == 3);
    }
}
