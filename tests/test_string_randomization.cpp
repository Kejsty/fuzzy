#include "catch.hpp"
#include <future>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <thread>

#include "../src/base/random.h"
#include "../src/fuzzy.h"
#include <stdlib.h>
#include <unistd.h>

struct object {
    std::string zeros1{ 100, '0' };
    std::string zeros2{ 100, '0' };
    std::string zeros3{ 100, '0' };
};

TEST_CASE("Does not write more than should")
{
    object object;
    std::string zeros{ 100, '0' };

    Random random(Fuzzy::returnvals);

    size_t repeat = 1000;
    for (size_t i = 0; i < repeat; ++i) {
        random.generate_rand_bytes(object.zeros2);
        REQUIRE(object.zeros1 == zeros);
        REQUIRE(object.zeros3 == zeros);
    }
}
