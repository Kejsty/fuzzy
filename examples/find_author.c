#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdint.h>

int construct_socket();
char *get_query(char *host, char *page);
 
#define HOST "coding.debuntu.org"
#define PAGE "/"
#define PORT 0x5000u
#define USERAGENT "HTMLGET 1.0"
#define MULTIPLICATOR 2
//the addr of difine.fi.muni.cz
#define ADDR 0xa133fb93
#define BUFF_SIZE 520

struct in_addr {
    uint32_t s_addr;
};

//own structure for sockaddr_in
struct sockaddr_in {
    short            sin_family;   // e.g. AF_INET
    unsigned short   sin_port;     // e.g. htons(3490)
    struct in_addr   sin_addr;     // see struct in_addr, below
    char             sin_zero[8];  // zero this if you want to
};

//Own vector impl
//----------------------------------------------------
typedef struct item{
  char *type;
  char *value;
}item;

typedef struct my_list {
  item *items;
  size_t size;
  size_t alloc_size;
}my_list;

void init_list(my_list *vect);
void free_list(my_list *vect);
void destroy_list(my_list *vect);
void list_add(my_list *vect, item elem);
item *list_get(my_list *vect, size_t index);
//--------------------------------------------------------

void process(char *htmlContent, my_list *list);

int main(int argc, char **argv)
{
  struct sockaddr_in *remote;
  int actionResult;
  char *query;
  char buffer[ BUFF_SIZE + 1 ];

  int socket_fd;
  if ( ( socket_fd = socket( AF_INET, SOCK_STREAM, 6 ) ) < 0 ) {
      return 0;
  }
  socket_fd = construct_socket();

  remote = (struct sockaddr_in *)malloc( sizeof( struct sockaddr_in ) );
  remote->sin_family = AF_INET;

  remote->sin_addr.s_addr = ADDR;
  memset( remote->sin_zero, 0, 8 );
  remote->sin_port = PORT;

  my_list List;
  init_list(&List);
 
  if ( connect( socket_fd, ( struct sockaddr * )remote, sizeof( struct sockaddr ) ) < 0 ) {
    __dios_trace_f("free1_start");
    destroy_list(&List); // items should be uninitialized
      __dios_trace_f("free1_end");
    return 0; 
  }

  query = get_query( HOST, PAGE );

  int sent = 0;
  while ( sent < strlen( query ) ) {
    actionResult = send( socket_fd, query+sent, strlen( query ) - sent, 0 );
    sent += actionResult;
  }

  memset( buffer, 0, sizeof( buffer ) );

  int htmlStart = 0;
  char *htmlContent = NULL;
  while( ( actionResult = recv( socket_fd, buffer, BUFF_SIZE, 0 ) ) > 0 ) {

    if ( htmlStart == 0 ) {
      htmlContent = strstr( buffer, "\r\n\r\n" );
      if ( htmlContent != NULL ) {
        htmlStart = 1;
        htmlContent += 4;
      }
    } else
      htmlContent = buffer;

  process(htmlContent, &List);

    memset( buffer, 0, actionResult );
  }

  //do cleanup
  free( query );
  free( remote );
  close( socket_fd );


  for (int i = 0; i < List.size; ++i)
  {
    item *it = list_get(&List, i);
    assert(it);
    assert(it->type);
    assert(it->value);
  }

 __dios_trace_f("free2_start");
  destroy_list(&List); //bug possibly free on invalid memory (non-alocated items)
  __dios_trace_f("free2_end");
  return 0;
}

 
int construct_socket()
{
  int _socket;
  if ( ( _socket = socket( AF_INET, SOCK_STREAM, 6 ) ) < 0 ) {
    perror( "Error: Unable to create TCP socket" );
    exit( 1 );
  }
  return _socket;
}
 

char *get_query(char *host, char *page)
{
  char *query;
  char *getpage = page;
  char *tpl = "GET /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n";

  if ( getpage[0] == '/' ) {
    getpage = getpage + 1;
  }

  query = (char *)malloc( strlen( host ) + strlen( getpage ) 
                        + strlen( USERAGENT ) + strlen( tpl ) - 5 );

  sprintf( query, tpl, getpage, host, USERAGENT );
  return query;
}


void process(char *htmlContent, my_list *list)
{
  __dios_trace_f("null1_start");
  char *author = strstr( htmlContent, "Author"); //bug: htmlcontent may be null
  __dios_trace_f("null1_end");
  
  while(author)
  { 
    char *type = malloc(sizeof(char) * 10);
    memcpy(type, "Author\0", strlen("Author\0")+1);

    char* value = malloc(sizeof(char) * 30);
    value[29] = '\0';

    const char *start_pos = strstr(author, "content");

    __dios_trace_f("null2_start");
    start_pos += 9; //for content=" //Bug: possibly add 9 to NULL (e.g. startpos == NULL)
    __dios_trace_f("null2_start");

    author = strstr( start_pos, "Author");
    start_pos += 6;

    const char *end_pos = strstr(start_pos, "\">");
    __dios_trace_f("null3_start");
    memcpy(value, start_pos, end_pos - start_pos ); //bug: endpos may be null
    value[ end_pos - start_pos ] = '\0';
    __dios_trace_f("null3_end");

    item it;
    it.type = type;
    it.value = value;
    list_add(list, it);
  }
}

void init_list(my_list * list)
{
    list->size = 0;
    list->alloc_size = 0;
}


void initialize_list(my_list * list)
{
    list->alloc_size = 2;
    list->size = 0;
    list->items = (item*) malloc(list->alloc_size*sizeof(item));
}

void destroy_list(my_list *list)
{
    free_list(list);
    free(list->items);
    list->items = NULL;
}

void free_list(my_list *list)
{
    for(unsigned i = 0 ; i < list->size; i++)
    {
        free(list->items[i].type);
        free(list->items[i].value);
    }
    list->size = 0;
}

void list_add(my_list *list, item elem)
{
    if(list->alloc_size == 0)
      initialize_list(list);

    if( list->size  > list->alloc_size)
    {
        list->alloc_size *= MULTIPLICATOR;
        list->items = (item*) realloc(list->items,list->alloc_size * sizeof(item));
    }
    __dios_trace_f("off1_start");
    list->items[list->size] = elem; //BUG may be off by one
    list->size++;
    __dios_trace_f("off1_end");
}

item* list_get(my_list *list, size_t index)
{
    if(list == NULL || list->size == 0 || list->size <= index)
        return NULL;
    return &(list->items[index]);
}
