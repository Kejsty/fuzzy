/** Errors:
off1
off2
off3
free1
**/
#include <pthread.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <assert.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdbool.h>
#include <errno.h>

#define SIZE 9

// #define MAX_BUF 1024

const char* board_data = 
"--***----------------****--------------------*--------*--------*---**---*--------";

enum Item
{
	empty,     // contains water
	occupied, // contains a ship
	hit		  // shot and hit 
};

typedef enum Item Item;

Item *board; // Two-dimensional array for gameboard.


Item switch_to_enum(char from)
{
	switch(from)
	{
		case '*': return occupied;
		default: return empty;
	}
}

void proccess_data()
{
	for (int i = 0; i < SIZE; ++i)
	{
		for (int j = 0; j < SIZE; ++j)
		{
			board[i*SIZE + j] = switch_to_enum(board_data[i*SIZE + j]);
		}
	}
}


void initialize_board( ) // Function to initialize the gameboard.
{
  // create a blank board   
  board = (Item*)malloc(SIZE * SIZE * sizeof(Item *));
  proccess_data();
}

void destruct_board() 
{
  free(board);
}

bool valid_input(char x, char y) 
{
	return isdigit(y) && isalpha(x) && isupper(x);
}

bool fight(char x, char y) 
{
	if(!isdigit(y) || !isalpha(x))
		return false;

	int x_coor = x - 'A'; // x in A-I range
	int y_coord = y - '1'; //y in. 1 - 9 range

	__dios_trace_f("off1_start");
	//This is artifical check because this bug could 
	//be ommited if the memory would be valid 
	assert(y_coord < SIZE && y_coord >= 0);// y_coord may be -1
	__dios_trace_f("off1_end");

	__dios_trace_f("off2_start"); 
	//This is artifical check because this bug could 
	//be ommited if the dynamic array is allocated in
	// one contious sequence
	assert(x_coor < SIZE && x_coor >= 0);// x_coor may be value greater than 9
	__dios_trace_f("off2_end");

    Item* item = board + (y_coord*SIZE + x_coor);
	
	if (*(item) == occupied) 
	{
		item[x_coor] = hit;
		return true;	
	}
	return false;
}

static int counter = 0;
static int attempts = 0;

void finalize()
{
	destruct_board();
}

void* got_attacked(void* fds_) 
{
    initialize_board();
    char attacks[4] = {0};
    
    int *fds = (int*) fds_;
    int fd_read = fds[0];
    int fd_write = fds[1];

	for (int i = 0; i < 10; ++i)
	{
		int readed = 0;
		while(!readed || (readed == -1 && errno == EAGAIN))
		{
			readed = read(fd_read, attacks, 3);
		}

		if(readed == -1)
		{
			//Some error
			perror("Reading error");
			break;
		}

		if(readed == 1 && attacks[0] == 'E')
		{
			/**
			here we wont add end, as its clear that 
			calling this will cause double free
			and the free part is non-atomic
			**/
			__dios_trace_f("free1_start");
			finalize();	
			break;
		}

		attempts++;
		if(fight(attacks[0], attacks[1]))
		{
			counter++;
			write(fd_write, "Y", strlen("Y"));
		}
		else
		{
			write(fd_write, "N", strlen("N"));
		}

	}

	finalize();
	return NULL;
}

#define MAXSHIPSIZE 4

void* attack(void* fds_)
{
	int *fds = (int*) fds_;
    int fd_read = fds[0];
    int fd_write = fds[1];

    //Represents some strategy
    const char* attacks[6][MAXSHIPSIZE] = {{"A2","A3","A4", "A4"},{"C1","D1","E1","F1"},{"E2", "D2", "C2", "B2"},{"E4", "E5", "E6", "E7"},{"F3", "G3", "H3", "I3"},{"A6", "A7", "A8", "A9"}};

    int x_ = 0;
    int y_ = 0;

    for (int i = 0; i < 10; ++i)
	{
		if(x_ >= 6)
		{
			write(fd_write, "E", strlen("E")); // send info about ending
			break;
		}

		__dios_trace_f("off3_start %d", y_); 
		//This is artifical check because this bug could 
		//be ommited
		assert(y_ < MAXSHIPSIZE && y_ >= 0);// y_ may be value greater than 3
		__dios_trace_f("off3_end");
		write(fd_write, attacks[x_][y_], strlen(attacks[x_][y_]));

		int readed = 0;
		char result;
		while(!readed || (readed == -1 && errno == EAGAIN))
		{
			readed = read(fd_read, &result, 1);
		}

		if(readed == -1)
		{
			//Some error
			perror("Reading error");
			continue;
		}

		if(result == 'N')
		{
			y_ = 0;
			x_++;
		}
		else
		{
			y_++;
		}
	}
    return NULL;
}

int main()
{
	int fd1[2];
	int fd2[2];
 	pipe(fd1);
 	pipe(fd2);

 	int fd_a[2] = {fd1[0], fd2[1]};
 	int fd_b[2] = {fd2[0], fd1[1]};

 	fcntl( fd1[0], F_SETFL, fcntl(fd1[0], F_GETFL) | O_NONBLOCK);
 	fcntl( fd2[0], F_SETFL, fcntl(fd2[0], F_GETFL) | O_NONBLOCK);
 	pthread_t thread_a;
 	pthread_t thread_b;
 	pthread_create( &thread_a, NULL , attack, fd_a);
    pthread_create( &thread_b, NULL , got_attacked, fd_b);

    pthread_join( thread_a, NULL );
    pthread_join( thread_b, NULL );

    //Cleanup
    close(fd1[0]);
    close(fd1[1]);
    close(fd2[0]);
    close(fd2[1]);

    //print statistics
	printf("Attempts: %d\n", attempts);
	printf("Attacks: %d\n", counter);


    return 0;
}
