#include <pthread.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

void *worker( void *data ) {
    
    char* filename = (char*)data;
    int fd = open( filename, O_WRONLY | O_CREAT, 0666);

    if(fd < 0)
    {
        perror("Unable to open file for write");
        return NULL;
    }

    write(fd, filename, strlen(filename));
    int res = close(fd);

    int fd2 = open(filename, O_RDONLY | O_CREAT, 0644 );
    if(fd2 < 0)
    {
        perror("Unable to open file for read");
        return NULL;
    }
    char buff[ 15 ] = {};
    memset(buff, 0, 15);

    int readed = read(fd2, buff, strlen(filename));
    close( fd2 );

    assert( readed == strlen(filename));
    assert( strcmp( buff, filename ) == 0 );
    return NULL;
}

int main() {

    pthread_t thread_a;
    pthread_create( &thread_a, NULL , worker, "a_file" );
    pthread_t thread_b;
    pthread_create( &thread_b, NULL , worker, "b_file" );

    pthread_join( thread_a, NULL );
    pthread_join( thread_b, NULL );
    return 0;
}
