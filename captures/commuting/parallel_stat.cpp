#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <vector>
#include <string>
#include <sys/stat.h>
#include <signal.h>
#include <thread>

const char* files_to_check[] = {"assgnsheets.pdf", "hw01_2018.pdf", "tlac"};
const int size = 3;
sig_atomic_t counter = -1;
sig_atomic_t directory = 0;
sig_atomic_t file = 0;
const char *dir = "/home/xkejstov/files" ;

void* statter(void* _)
{
	int next_to_process = ++counter;
	while(next_to_process < size)
	{
		struct stat stbuf;
		memset (&stbuf, 0, sizeof(stbuf));
		std::string fullpath = dir;
		fullpath += "/";
		fullpath += files_to_check[next_to_process];
		 if( stat(fullpath.c_str(),&stbuf ) == -1 )
		{
			std::string err = "Unable to stat file: ";
			err += fullpath;
		   perror(err.c_str()) ;
		}
		else
		{
			if ( ( stbuf.st_mode & S_IFMT ) == S_IFDIR )
		  	{
			   	++directory;
		  	}
		  	else
		  	{
		  		++file;
		  	}
		}
	  	next_to_process = ++counter;
	}
	return NULL;
}

int main()
{

	pthread_t thread_a;
 	pthread_t thread_b;
 	pthread_create( &thread_a, NULL , statter, NULL);
    pthread_create( &thread_b, NULL , statter, NULL);

    pthread_join( thread_a, NULL );
    pthread_join( thread_b, NULL );

	printf("Directories: %d, Files: %d\n", directory, file);
    return 0;
}
