#include <pthread.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <signal.h>

const size_t process = 128;
const int portion = 64;

sig_atomic_t counter = 0;
const char *data = "Consider an example program provided in Fig. ref{fig:inode_rw_example}. \
 A result of this program depends on that, whether the paths  of a.txt and b.txt directs to \
 the same file or not. If not, we do not actually care, whether some write of thread A will \
 be executed before some write of thread B and vice versa. However, if their points to the \
 same file, the result of this program is non-deterministic, as every character textit{-} \
 may be followed either by character textit{a} or character textit{b}. Moreover, during the \
 replay of captured system call trace, we are unable to decide whether an write of thread A \
 can be executed before a write of thread B, as this may change the resulting file in a way \
 that is unknown to us - in a general case.";




void *worker( void *_ ) {
    int next_id = counter++;

    while(next_id < process / portion)
    {
        char filename[24];
        sprintf(filename, "partition/processed_%d", next_id);
        int fd = open( filename, O_WRONLY | O_CREAT, 0666);

        if(fd < 0)
        {
            perror("Unable to open file for write");
            continue;
        }
       
        write(fd, data + next_id*portion, portion);
        int res = close(fd);
        next_id = counter++;
    }
    return _;
}

int main() {
    assert(strlen(data) >= 256);

    struct stat st = {0};
    if (stat("partition", &st) == -1) {
        mkdir("partition", 0700);
    }

    pthread_t thread_a;
    pthread_create( &thread_a, NULL , worker, NULL );
    pthread_t thread_b;
    pthread_create( &thread_b, NULL , worker, NULL );

    pthread_join( thread_a, NULL );
    pthread_join( thread_b, NULL );
    return 0;
}
