#!/bin/bash

if [[ $1 && $2 ]]
then
    if ! [ "$2" -eq "$2" ] 2> /dev/null
    then
        echo "Sorry integers only"
    else
    	echo "COVERAGE:"
    	name="coverage_$1_capture_$2"
    	unzip $name.zip -d $name &> /dev/null
    	sleep 8s
	cp errors $name/fuzzy_working_dir/
    	./statistics $name/fuzzy_working_dir/
    	sleep 4s
    	rm -rf $name
    
    	echo "FANOUT:"
    	name="fanout_$1_capture_$2"
    	unzip $name.zip -d $name &> /dev/null
    	sleep 8s
	cp errors $name/fuzzy_working_dir/
    	./statistics $name/fuzzy_working_dir/
    	sleep 4s
    	rm -rf $name
    
    	echo "LONGEST:"
    	name="longest_$1_capture_$2"
    	unzip $name.zip -d $name &> /dev/null
    	sleep 8s
	cp errors $name/fuzzy_working_dir/
    	./statistics $name/fuzzy_working_dir/
    	sleep 4s
    	rm -rf $name
    
    	echo "ALL:"
    	name="all_$1_capture_$2"
    	unzip $name.zip -d $name &> /dev/null
    	sleep 8s
	cp errors $name/fuzzy_working_dir/
    	./statistics $name/fuzzy_working_dir/
    	sleep 4s
    	rm -rf $name
     fi
else
     echo "USAGE: fuzzy_approach time_to_run"
fi
