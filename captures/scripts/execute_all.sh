#!/bin/bash

if [[ $1 && $2 ]]
then
	if ! [ "$2" -eq "$2" ] 2> /dev/null
	then
	    echo "Sorry integers only"
	else
		echo "Running ALL for: $2 seconds"

		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select coverage --fuzzy returnval --time $2
		zipname="coverage_returnval_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select longest --fuzzy returnval --time $2
		zipname="longest_returnval_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select fanout --fuzzy returnval --time $2
		zipname="fanout_returnval_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select all --fuzzy returnval --time $2
		zipname="all_returnval_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/

		# ---------------------------------------------------------------------------------------------

		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select coverage --fuzzy random --time $2
		zipname="coverage_random_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select longest --fuzzy random --time $2
		zipname="longest_random_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select fanout --fuzzy random --time $2
		zipname="fanout_random_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select all --fuzzy random --time $2
		zipname="all_random_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/

		# ---------------------------------------------------------------------------------------------

		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select coverage --fuzzy string --time $2
		zipname="coverage_string_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select longest --fuzzy string --time $2
		zipname="longest_string_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select fanout --fuzzy string --time $2
		zipname="fanout_string_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select all --fuzzy string --time $2
		zipname="all_string_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/

		# ---------------------------------------------------------------------------------------------


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select coverage --fuzzy bits --time $2
		zipname="coverage_bits_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select longest --fuzzy bits --time $2
		zipname="longest_bits_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select fanout --fuzzy bits --time $2
		zipname="fanout_bits_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $2s ./fuzzier -f $1  -d test/divine -m release --select all --fuzzy bits --time $2
		zipname="all_bits_capture_$2.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


	fi
else
	echo "USAGE: input_file time_to_run"
fi
