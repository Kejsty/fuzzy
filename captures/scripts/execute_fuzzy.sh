#!/bin/bash

if [[ $1 && $2 && $3 ]]
then
	if ! [ "$3" -eq "$3" ] 2> /dev/null
	then
	    echo "Sorry integers only"
	else
		echo "Running for: $3 seconds"

		timeout $3s ./fuzzier -f $1  -d test/divine -m release --select coverage --fuzzy $2 --time $3
		zipname="coverage_$2_capture_$3.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $3s ./fuzzier -f $1  -d test/divine -m release --select longest --fuzzy $2 --time $3
		zipname="longest_$2_capture_$3.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $3s ./fuzzier -f $1  -d test/divine -m release --select fanout --fuzzy $2 --time $3
		zipname="fanout_$2_capture_$3.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/


		timeout $3s ./fuzzier -f $1  -d test/divine -m release --select all --fuzzy $2 --time $3
		zipname="all_$2_capture_$3.zip"
		zip -r $zipname fuzzy_working_dir/
		sleep 15s
		rm -rf  fuzzy_working_dir/
	fi
else
	echo "USAGE: input_file fuzzy_approach time_to_run"
fi
