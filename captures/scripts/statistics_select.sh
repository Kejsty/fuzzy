if [[ $1 && $2 ]]
then
    if ! [ "$2" -eq "$2" ] 2> /dev/null
    then
        echo "Sorry integers only"
    else
    	echo "BITS:"
    	name="$1_bits_capture_$2"
    	unzip $name.zip -d $name &> /dev/null
    	sleep 8s
	    cp errors $name/fuzzy_working_dir/
    	./statistics $name/fuzzy_working_dir/
    	sleep 4s
    	rm -rf $name
    
        echo "RANDOM:"
        name="$1_random_capture_$2"
        unzip $name.zip -d $name &> /dev/null
        sleep 8s
        cp errors $name/fuzzy_working_dir/
        ./statistics $name/fuzzy_working_dir/
        sleep 4s
        rm -rf $name

        echo "STRING:"
        name="$1_string_capture_$2"
        unzip $name.zip -d $name &> /dev/null
        sleep 8s
        cp errors $name/fuzzy_working_dir/
        ./statistics $name/fuzzy_working_dir/
        sleep 4s
        rm -rf $name

        echo "RETURN:"
        name="$1_return_capture_$2"
        unzip $name.zip -d $name &> /dev/null
        sleep 8s
        cp errors $name/fuzzy_working_dir/
        ./statistics $name/fuzzy_working_dir/
        sleep 4s
        rm -rf $name
     fi
else
     echo "USAGE: select_approach time_to_run"
fi
