if [[ $1 ]]
then
    if ! [ "$1" -eq "$1" ] 2> /dev/null
    then
        echo "Sorry integers only"
    else
    	echo "ALL"
        ./statistics_select.sh all $1


        echo "FANOUT"
        ./statistics_select.sh fanout $1


        echo "LONGEST"
        ./statistics_select.sh longest $1 


        echo "COVERAGE"
        ./statistics_select.sh coverage $1
     fi
else
     echo "USAGE: time_to_run"
fi
