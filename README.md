# Fuzzy

Fuzzier - part of my diploma thesis. This software ensures model checking of the provided program with the help of `DIVINE`, and program trace. This program trace (in a form of FS calls dumps) is then modified (with the fuzzy method and deterministic changes) and used to enhance state-space covered by `DIVINE`.

This method is applied repeatedly, and always only the best (and most successful) traces are selected for next round of fuzzing (with some randomnes, ofcourse).