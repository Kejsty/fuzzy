#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

std::vector<std::string> bugs;
std::vector<bool> found;
std::vector<int> at_time;
//std::unordered_map<std::string, size_t> bug_counter;

void process_graph(std::ifstream& graph, int counter);

int main(int argc, char** argv)
{
    std::string prefix;
    if (argc > 1) {
        prefix = argv[1];
    }

    std::cout << "Running with prefix: " << prefix << std::endl;

    std::ifstream errors(prefix + "/errors");
    if (!errors) {
        std::cerr << "No errors defined!" << std::endl;
        return 1;
    }

//    at_time.resize()

    int bug_id = 0;
    std::string line;
    while (!errors.eof()) {
        std::getline(errors, line);
        if (!line.empty()) {
            bugs.push_back(line);
            at_time.push_back(-1);
            found.push_back(false);
            ++bug_id;
        }
    }

    int counter = 0;
    int not_in_a_row = 0;
    while (true) {
        std::string input_name = prefix + "/modify" + std::to_string(counter) + "_graph";
        std::ifstream graph(std::move(input_name));

        if (!graph)
        {
            not_in_a_row++;
        }
        else
        {
            not_in_a_row = 0;
            process_graph(graph, counter);
        }

        if(not_in_a_row >= 10)
            break;

        ++counter;
    }

    std::cout << "Statistics:\n";
    for(int bug_id = 0; bug_id < bugs.size(); ++bug_id ) {
        std::cout << "\t" << bugs[bug_id] << ": " << at_time[bug_id] << std::endl;
    }
}

void process_graph(std::ifstream& graph, int counter_)
{
    std::vector<int> counter;
    counter.resize(bugs.size(), 0);
    std::string line;
    while (!graph.eof()) {
        std::getline(graph, line);
        for(int bug_id = 0; bug_id < bugs.size(); ++bug_id ) {
            if(found[bug_id])
                continue;
            if (line.find("(0) null2_start\\l(0) null2_start\\l") != std::string::npos)
                continue;

            if (line.find(bugs[bug_id] + "_start") != std::string::npos)
                ++counter[bug_id];

            if (line.find(bugs[bug_id] + "_end") != std::string::npos) {
                --counter[bug_id];
            }
        }
    }

    for(int bug_id = 0; bug_id < bugs.size(); ++bug_id ) {
        if(found[bug_id])
            continue;

        if(counter[bug_id] > 0)
        {
            found[bug_id] = true;
            at_time[bug_id] = counter_;
        }
    }
}
