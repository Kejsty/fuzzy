#include <array>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <random>

#include "../external/optparse/optparse.h"

enum Mode { Run,
	Draw,
	Verify,
	None };

std::ostream &me(std::ostream &out_ = std::cout) {
	out_ << "[Divine Simulator]: ";
	return out_;
}

[[noreturn]] void fail(const std::string why) {
	me(std::cerr) << why << std::endl;
	exit(1);
}

Mode getMode(const std::vector<std::string> &args) {
	for (const auto &arg : args) {
		if (arg == "exec")
			return Run;
		if (arg == "draw")
			return Draw;
		if (arg == "verify")
			return Verify;
	}
	return None;
}

bool copy_file(const char *srce_file, const char *dest_file) {
	std::ifstream srce(srce_file, std::ios::binary);
	if (!srce) {
		me() << "Error while oppening{SRC}: " << srce_file << std::endl;
		return false;
	}
	std::ofstream dest;
	dest.open(dest_file, std::fstream::out | std::fstream::trunc);
	if (!dest) {
		me() << "Error while oppening{DST}: " << dest_file << std::endl;
		return false;
	}

	dest << srce.rdbuf();
	return true;
}

namespace files {
static const std::string prefix = "examples/simulation/";
static const std::string passthough = prefix + "passthrough.out";

static std::string capture(size_t index)
{
    return prefix + "modify" + std::to_string(index) + "_graph";
}
} // namespace files

[[noreturn]] void prepare_verify(const std::vector<std::string> &) {
	fail("Not expected - Verify mode");
}

void prepare_draw(const std::vector<std::string> &args) {
	std::string graph_name;
	for (const auto &arg : args) {
		if (arg.find("tee ") == 0) {
			graph_name = arg.substr(arg.find("tee ") + 4);
			break;
		}
	}

	me() << "Running in draw mode with output: " << graph_name << std::endl;

	if (graph_name.empty())
		fail("incorrect graph name provided");
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(0, 1150);
    size_t grahp_to_pick = dis(gen);
    if (!copy_file(files::capture(grahp_to_pick).c_str(), graph_name.c_str()))
		fail("Error while copying file");
}

void prepare_run(const std::vector<std::string> &) {
	me() << "Running in run mode!\n";

    if (!copy_file(files::passthough.c_str(), "passthrough.out"))
		fail("Error while copying file (passthrough.out)");
}

int main(int argc, char **argv) {
	optparse::OptionParser parser = optparse::OptionParser().description(
	    "Welcome in divine simulator. It's used for ");

	optparse::OptionGroup group = optparse::OptionGroup("-O option groups");
	group.add_option("-o").action("store_true").help("Group -o Option.");
	parser.add_option_group(group);

	parser.add_option("-rf", "--report-filename")
	    .dest("report_file")
	    .help("path to Divine extecutable")
	    .set_default("/test/divine");

	parser.add_option("-re", "--render")
	    .dest("render")
	    .help("Render graph with this command")
	    .action("store_true");

    parser.add_option("-re", "--distance")
        .dest("distance")
        .help("distance for draw")
        .action("store_true");

	const optparse::Values options = parser.parse_args(argc, argv);
	const std::vector<std::string> args = parser.args();

	Mode mode = getMode(args);

	if (mode == None)
		fail("No mode recognized, can't continue!");

	switch (mode) {
	case Verify:
		prepare_verify(args); // fails with exit
	case Run:
		prepare_run(args);
		break;
	case Draw:
		if (!options.get("render"))
			fail("No render option provided for DRAW option");
		prepare_draw(args);
		break;
	default:
		fail("Nothing to do");
	}

	return 0;
}
