#include <future>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <thread>

#include <cstdlib>

#include "../external/optparse/optparse.h"
#include "../src/base/config.h"
#include "../src/launcher.h"

int main(int argc, char** argv)
{

    optparse::OptionParser parser = optparse::OptionParser()
                                        .description("Welcome in fuzzier, an program that fuzzies system "
                                                     "calls of DIVINE")
                                        .usage("fuzzier -d [Path-To-Divine] -f [File-To-Verify]");

    parser.add_option("-d", "--divine")
        .dest("divine")
        .help("path to Divine extecutable")
        .set_default("/test/divine");
    parser.add_option("-m", "--mode")
        .dest("exe_mode")
        .help("mode of divine execution if some [release,debug]")
        .set_default("");

    parser.add_option("--zip-into")
        .dest("zip_file")
        .help("ostput zip file")
        .set_default("captures.zip");

    parser.add_option("-f", "--file")
        .dest("filename")
        .help("program to fuzzy")
        .metavar("FILE");
    parser.add_option("-q", "--quiet")
        .action("store_false")
        .dest("verbose")
        .set_default("1")
        .help("don't print status messages to stdout");

    parser.add_option("--outputs")
        .set_default(10)
        .dest("fuzzyperround")
        .help("number of different fuzzy files produced per one round")
        .type("int");

    parser.add_option("--to-next")
        .set_default(3)
        .dest("takentonext")
        .help("number best graphs (wrt coverage) that are taken to next round")
        .type("int")
        .metavar("NUM");

    parser.add_option("-t", "--time")
        .dest("time")
        .help("approximate time for which the fuzzer runs in seconds")
        .type("int")
        .metavar("NUM");

    std::vector<std::string> select{ "all", "coverage", "longest", "fanout" };

    parser.add_option("--select")
        .set_default("coverage")
        .dest("select")
        .help("approach in selecting traces for next run: [all,coverage,longest,fanout]")
        .type("choice")
        .choices(select.begin(), select.end());

    std::vector<std::string> fuzzy{ "bits", "random", "string", "return" };

    parser.add_option("--fuzzy")
        .set_default("string")
        .dest("fuzzy")
        .help("approach in fuzzing traces for next run [bits,random,string,return]")
        .type("choice")
        .choices(fuzzy.begin(), fuzzy.end());

    const optparse::Values options = parser.parse_args(argc, argv);
    const std::vector<std::string> args = parser.args();

    if (!options.is_set("divine") || !options.is_set("filename")) {
        if (!options.is_set("divine")) {
            std::cout << "Missing executable to Divine";
        }

        if (!options.is_set("filename")) {
            std::cout << "Missing filename of program";
        }

        parser.print_help();
        return 1;
    }

    Config config;

    config.path_to_divine = options["divine"];
    if (config.path_to_divine[0] != '/') {
        // make it executable
        config.path_to_divine = "/" + config.path_to_divine;
    }

    config.program = options["filename"];
    if (options.is_set("exe_mode"))
        config.mode_of_execution = options["exe_mode"];

    config.quiet = options.get("verbose");
    config.fuzzy_per_round = static_cast<size_t>(options.get("fuzzyperround"));
    config.taken_to_next = static_cast<size_t>(options.get("takentonext"));

    if (options.is_set("time")) {
        std::chrono::seconds time(static_cast<size_t>(options.get("time")));
        config.time_to_run = time;
    }

    config.selection_approach = string_to_select(static_cast<const char*>(options.get("select")));
    config.fuzzy_approach = string_to_fuzzy(static_cast<const char*>(options.get("fuzzy")));

    std::cout << "Running with: \n"
              << "\t Fuzzy per round: " << config.fuzzy_per_round
              << "\n\t Taken to next round: " << config.taken_to_next
              << "\n\t Time to run: " << config.time_to_run.count()
              << "\n\t Fuzzy approach: " << static_cast<const char*>(options.get("fuzzy"))
              << "\n\t Select approach: " << static_cast<const char*>(options.get("select"))
              << std::endl;

    Launcher launcher(std::move(config));
    launcher.start();

    return 0;
}
