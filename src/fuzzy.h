#pragma once
#include "base/config.h"
#include "base/random.h"
#include "trace.h"

#include <netinet/in.h>

#include <map>

//for tests
std::tuple<int, int> position_in_read(SyscallSnapshot& read_s);

/**
 * @brief find_modifiable_parts generate new traces
 * @param input
 * @param name_seed
 * @param num_of_traces
 * @return
 */

class Fuzzier {
public:
    Fuzzier(std::string name_prefix_, Config cfg_)
        : _name_prefix(std::move(name_prefix_))
        , _cfg(std::move(cfg_))
        , random(_cfg.fuzzy_approach)
    {
    }

    using string_map = std::map<std::string, std::string>;

    size_t current_counter() const
    {
        return _counter;
    }

    const std::vector<std::string>& generate_new_traces(std::vector<SyscallSnapshot> syscalls)
    {
        _output_file_names.clear();
        for (size_t i = 0; i < _cfg.fuzzy_per_round; ++i) {
            generate_new_trace(syscalls); // copy is made
        }
        return _output_file_names;
    }

    std::vector<std::string> find_modifiable_parts(const std::string& input);

    //for tests
    void modify_bytes(std::string& where, size_t position_from, size_t length);

private:
    std::vector<SyscallSnapshot> process(std::vector<SyscallSnapshot>);

    void generate_new_trace(std::vector<SyscallSnapshot> syscalls)
    {
        auto modified = process(std::move(syscalls));
        std::string out_file{ _name_prefix + std::to_string(_counter) };
        ++_counter;
        serialize(modified, out_file);
        _output_file_names.push_back(out_file);
    }

    const std::vector<std::string>& get_new_fuzzy_files() const
    {
        return _output_file_names;
    }

    void modify_data(SyscallSnapshot& snapshot_);
    void modify_errno(SyscallSnapshot& snapshot_);
    void modify_struct(std::string& where, size_t position_from, size_t length);
    void modify_socket_struct(std::string& where, size_t position_from, size_t length);
    void modify_return(SyscallSnapshot& snapshot_);

private:
    std::vector<std::string> _output_file_names;
    static size_t _counter;
    std::string _name_prefix;
    Config _cfg;
    std::map<std::string, std::string> _fuzzied_structures;
    Random random;
};
