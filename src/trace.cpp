#include "trace.h"
#include <fstream>

bool makeError()
{
    std::cerr << "Wrong format!\n";
    return false;
}

void print(std::vector<SyscallSnapshot>& snapshots)
{
    for (auto& snapshot : snapshots) {
        std::cout << "Syscall: \n\t number: " << snapshot._number
                  << "\n\tinode: " << snapshot._inode
                  << "\n\terrno: " << snapshot._errno << std::endl;
    }
}

void serialize_syscall(std::ofstream& out, const SyscallSnapshot& snapshot)
{
    size_t inputs_s = snapshot._inputs.length();
    out << "SYSCALL:";
    out.write(reinterpret_cast<const char*>(&snapshot._number), sizeof(int));
    out.write(reinterpret_cast<const char*>(&snapshot._inode), sizeof(uint64_t));
    out.write(reinterpret_cast<char*>(&inputs_s), sizeof(size_t));
    out << snapshot._inputs;
    out.write(reinterpret_cast<const char*>(&snapshot._returnLength),
        sizeof(size_t));
    out.write(reinterpret_cast<const char*>(&snapshot._errno), sizeof(int));
}

void serialize(const std::vector<SyscallSnapshot>& snapshots,
    const std::string& output)
{
    std::ofstream out(output, std::ios::binary);

    for (const auto& snapshot : snapshots) {
        serialize_syscall(out, snapshot);
    }
}

bool getSyscall(std::ifstream& istrm, SyscallSnapshot& snapshot)
{

    char nameBuf[8];
    istrm.read(reinterpret_cast<char*>(nameBuf), sizeof(char) * 8); // SYSCALL:

    if (istrm.eof() || istrm.bad())
        return makeError();
    istrm.read(reinterpret_cast<char*>(&snapshot._number), sizeof(int));

    if (istrm.eof() || istrm.bad())
        return makeError();
    istrm.read(reinterpret_cast<char*>(&snapshot._inode), sizeof(uint64_t));

    if (istrm.eof() || istrm.bad())
        return makeError();
    size_t in_data_length;
    istrm.read(reinterpret_cast<char*>(&in_data_length), sizeof(size_t));

    if (istrm.eof() || istrm.bad())
        return makeError();
    char inputData[in_data_length];
    istrm.read(reinterpret_cast<char*>(inputData),
        sizeof(char) * in_data_length);
    snapshot._inputs = std::string(inputData, in_data_length);

    if (istrm.eof() || istrm.bad())
        return makeError();
    istrm.read(reinterpret_cast<char*>(&snapshot._returnLength), sizeof(size_t));

    if (istrm.eof() || istrm.bad())
        return makeError();
    istrm.read(reinterpret_cast<char*>(&snapshot._errno), sizeof(int));

    if (istrm.bad())
        return makeError();
    return true;
}

std::vector<SyscallSnapshot> parse_input(const std::string& input, bool talk)
{
    std::ifstream istrm(input, std::ios::binary);
    int timestamp = 0;
    if (!istrm) {
        if (talk)
            std::cout << "Failure while parsing!\n";
        return {};
    }

    std::vector<SyscallSnapshot> snapshots;
    while (true) {
        SyscallSnapshot next;
        if (!getSyscall(istrm, next)) {
            if (talk)
                std::cout << "Failure while parsing!\n";
            return {};
        }

        next._timestamp = timestamp++;
        snapshots.push_back(next);

        if (istrm.eof() || istrm.peek() == EOF) {

            return snapshots;
        }
    }
}
