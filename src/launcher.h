#pragma once
#include <cstdlib>
#include <future>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <thread>

// Local
#include "base/config.h"
#include "base/select.h"
#include "fuzzy.h"
#include "graph.h"

class Launcher {
    using graph_pair = std::pair<std::string, std::unique_ptr<divineGraph>>;

    enum state {
        None,
        Starting,
        Verify,
        Fuzzy,
        Graphs,
        Error,
        Finishing
    };

public:
    Launcher(Config config)
        : _config(std::move(config))
        , _selector(_config)
        , _fuzzier("fuzzy_working_dir/modify", _config)
    {
        srand(time(NULL));
    }

    void start();

private:
    void process_passthrough_output(std::string out_file_);
    std::string include_passtrough();
    void zip_result();
    bool validate_first_run();
    void print_end_info();
    bool have_time_elapsed();

    // The function we want to execute on the new thread.
    int run_draw_session(const std::string& id, std::string input);
    std::vector<std::string> make_round(std::vector<std::string> fuzzied_files_);

    void check_graphs(const std::vector<graph_pair>& graphs_);

private:
    friend std::ostream& operator<<(std::ostream& out_, state state_)
    {
        switch (state_) {
        case None:
            out_ << "None";
            break;
        case Verify:
            out_ << "Verify";
            break;
        case Starting:
            out_ << "Starting";
            break;
        case Fuzzy:
            out_ << "Fuzzy";
            break;
        case Graphs:
            out_ << "Graphs";
            break;
        case Error:
            out_ << "Error";
            break;
        case Finishing:
            out_ << "Finishing";
            break;
        }
        return out_;
    }

    friend std::ostream& operator<<(std::ostream& out_, Launcher& launcher_)
    {
        launcher_.me(out_);
        return out_;
    }

    std::ostream& me(std::ostream& out_ = std::cout)
    {
        out_ << "[Launcher (" << _state << ") iteration:" << _iteration << "]:";
        return out_;
    }

private:
    size_t _iteration{ 0 };
    state _state{ None };
    Config _config;
    Selector _selector;
    Fuzzier _fuzzier;
    std::set<std::string> errornous_traces;
};
