﻿#include "fuzzy.h"
#include "base/errors.h"
#include "base/parsing.h"
#include "base/random.h"
#include "trace.h"
#include <cstdlib>
#include <ctime>
#include <sys/syscall.h>
#include <vector>

/**
 * @brief position_in_read
 * As read system call is declared as
 * ssize_t read(int fd, void *buf, size_t nbyte);
 * We can find the modyfiable part after fd
 */
std::tuple<int, int> position_in_read(SyscallSnapshot& read_s)
{
    size_t move_for = 0;

    auto data = read_s._inputs;
    // move after fd
    parsing::primitive<int>(data);

    move_for += sizeof(int); // fd
    move_for += sizeof(int); // size of data

    auto length = parsing::length(data);
    // std::cout << "Parsed read: " << length << std::endl;

    return { move_for, length };
}

/**
 * @brief position_in_readlink
 * @param read_s
 * ssize_t readlink(const char *r_path, char *r_buf, size_t bufsize);
 * We can find the modyfiable part in r_buf
 */
std::tuple<int, int> position_in_readlink(SyscallSnapshot& readlink_s)
{
    auto data = readlink_s._inputs;

    size_t move_for = sizeof(int); // r_path_size
    move_for += parsing::memory_size(data); // r_path data
    move_for += sizeof(int); // size of data

    return { move_for, parsing::array_size(data) };
}

/**
 * @brief position_in_stat
 * As stat system call is declared as
 * int stat(const char *restrict_path, struct stat *restrict_buf);
 * We can find the modyfiable part in restrict_buf
 */
std::tuple<int, int> position_in_stat(SyscallSnapshot& read_s)
{
    size_t move_for = 0;

    auto data = read_s._inputs;
    move_for += sizeof(int); // size of data
    move_for += parsing::memory_size(data); // MEM data itself
    move_for += sizeof(bool); // is null flag

    return { move_for, parsing::structure_size<struct stat*>(data) };
}

/**
 * @brief position_in_fstat
 * As fstat system call is declared as
 * int fstat(int fildes, struct stat *buf);
 * We can find the modyfiable part in buf
 */
std::tuple<int, int> position_in_fstat(SyscallSnapshot& read_s)
{
    size_t move_for = 0;

    auto data = read_s._inputs;
    // move after fd
    parsing::primitive<int>(data);

    move_for += sizeof(int); // fd
    move_for += sizeof(bool); // is null flag

    return { move_for, parsing::structure_size<struct stat*>(data) };
}

/**
 * @brief position_in_lstat
 * As lstat system call is declared as
 * lstat(const char *restrict path, struct stat *restrict_buf);
 * We can find the modyfiable part in restrict_buf. The System call
 * look tha same as stat for our purpouses.
 */
std::tuple<int, int> position_in_lstat(SyscallSnapshot& read_s)
{
    return position_in_stat(read_s);
}

/**
 * @brief position_in_getcwd
 * As getcwd system call is declared as
 * char *getcwd(char *buf, size_t size);
 * We can find the modyfiable part in buf
 * TODO: we can fuzzy up to size!
 */
std::tuple<int, int> position_in_getcwd(SyscallSnapshot& read_s)
{
    size_t move_for = 0;

    auto data = read_s._inputs;
    // move after fd
    move_for += sizeof(int); // size of data

    return { move_for, parsing::length(data) };
}

/**
 * @brief position_in_recvfrom
 * As recvfrom system call is declared as
 *  ssize_t recvfrom(int socket, void *restrict_buffer, size_t length,
 *  int flags, struct sockaddr *restrict_address, socklen_t *restrict
 * address_len); We can find the modyfiable part in restrict_buffer
 * TODO: we can fuzzy restrict_address if it is not null!
 */
std::tuple<int, int> position_in_recvfrom(SyscallSnapshot& read_s)
{
    size_t move_for = 0;

    auto data = read_s._inputs;
    // move after socket
    parsing::primitive<int>(data);

    move_for += sizeof(int); // socket
    move_for += sizeof(int); // size of data

    return { move_for, parsing::length(data) };
}

/**
 * @brief position_in_recvfrom
 * As recvfrom system call is declared as
 *  ssize_t recvfrom(int socket, (count) void *restrict_buffer, size_t length,
 *  int flags, struct sockaddr *restrict_address, socklen_t *restrict
 * address_len);
 * @return position to the restrict_address and its length
 */
std::tuple<int, int> struct_in_recvfrom(SyscallSnapshot& read_s)
{
    size_t move_for = 0;

    auto data = read_s._inputs;
    // move after socket
    parsing::primitive<int>(data);

    move_for += sizeof(int); // socket

    move_for += sizeof(int); // size of restrict_buffer
    move_for += parsing::memory_size(data); // length of data

    parsing::primitive<size_t>(data);
    move_for += sizeof(size_t); // length

    parsing::primitive<int>(data);
    move_for += sizeof(int); // flags

    move_for += sizeof(int); // size

    return { move_for, parsing::socket_structure_size(data) };
}

std::vector<std::string>
Fuzzier::find_modifiable_parts(const std::string& input)
{
    auto syscalls = parse_input(input);
    return generate_new_traces(std::move(syscalls));
}

// -------------- Syscall_Processor --------------------------------

size_t Fuzzier::_counter = 0;

std::vector<SyscallSnapshot>
Fuzzier::process(std::vector<SyscallSnapshot> syscalls)
{
    if (CHOOSE()) {
        size_t which = rand() % syscalls.size();
        SyscallSnapshot copy = syscalls[which];
        syscalls.insert(syscalls.begin() + which, std::move(copy));
    }

    //swap 2 calls with same inode if lucky
    if (CHOOSE()) {
        size_t fst = rand() % syscalls.size();

        size_t attempts = 0;
        size_t max_attempts = rand() % 5;
        while (attempts < max_attempts) {
            size_t snd = rand() % syscalls.size();
            if (syscalls[fst]._inode == syscalls[snd]._inode) {
                std::iter_swap(syscalls.begin() + fst, syscalls.begin() + snd);
                break;
            }
            ++attempts;
        }
    }

    for (auto& snapshot : syscalls) {
        modify_errno(snapshot);
        modify_return(snapshot);
        modify_data(snapshot);
    }
    return syscalls;
}

void Fuzzier::modify_bytes(std::string& where, size_t position_from,
    size_t length)
{
    if (length <= 0)
        return;

    std::string to_modify = where.substr(position_from, length);
    random.fuzzy_string(to_modify);
    where.replace(position_from, length, to_modify);
}

void Fuzzier::modify_data(SyscallSnapshot& snapshot_)
{
    if (!random._data_dist(Random::gen))
        return;

    switch (snapshot_._number) {
#ifdef SYS_read
    case SYS_read: {
        size_t from, length;
        std::tie(from, length) = position_in_read(snapshot_);
        // assert(from+length <= snapshot_._inputs.length());
        modify_bytes(snapshot_._inputs, from, length);
    } break;
#endif
#ifdef SYS_stat
    case SYS_stat: {
        size_t from = 0, length = 0;
        std::tie(from, length) = position_in_stat(snapshot_);
        modify_bytes(snapshot_._inputs, from, length);
    } break;
#endif
#ifdef SYS_fstat
    case SYS_fstat: {
        size_t from = 0, length = 0;
        std::tie(from, length) = position_in_fstat(snapshot_);
        modify_bytes(snapshot_._inputs, from, length);
    } break;
#endif
#ifdef SYS_lstat
    case SYS_lstat: {
        size_t from = 0, length = 0;
        std::tie(from, length) = position_in_lstat(snapshot_);
        modify_bytes(snapshot_._inputs, from, length);
    } break;
#endif
#ifdef SYS_recvfrom
    case SYS_recvfrom: {
        size_t from = 0, length = 0;
        std::tie(from, length) = position_in_recvfrom(snapshot_);
        modify_bytes(snapshot_._inputs, from, length);
        std::tie(from, length) = struct_in_recvfrom(snapshot_);
        modify_socket_struct(snapshot_._inputs, from, length);
    } break;
#endif
#ifdef SYS_getcwd
    case SYS_getcwd: {
        size_t from = 0, length = 0;
        std::tie(from, length) = position_in_getcwd(snapshot_);
        modify_bytes(snapshot_._inputs, from, length);
    } break;
#endif
#ifdef SYS_readlink
    case SYS_readlink: {
        size_t from = 0, length = 0;
        std::tie(from, length) = position_in_readlink(snapshot_);
        modify_bytes(snapshot_._inputs, from, length);
    } break;
#endif
    }

    for (auto& structure : _fuzzied_structures) {
        auto pos = snapshot_._inputs.find(structure.first);
        if (pos != std::string::npos) {
            snapshot_._inputs.replace(pos, structure.first.size(), structure.second);
        }
    }
}

void Fuzzier::modify_errno(SyscallSnapshot& snapshot_)
{
    switch (_cfg.fuzzy_approach) {
    case Fuzzy::returnvals:
        break;
    default:
        return;
    }
    if (Random::errno_dist(Random::gen)) // switch the errno value with some P
    {
        while (true) {
            int error = Random::errno_values_dist(Random::gen);
            if (errors.find(error) != errors.end()) {
                snapshot_._errno = error;
                return;
            }
        }
    }
}

void Fuzzier::modify_struct(std::string& where, size_t position_from,
    size_t length)
{
    if (Random::struct_dist(Random::gen)) // switch the errno value with some P
    {
        if (length <= 0)
            return;

        std::string to_modify = where.substr(position_from, length);
        random.fuzzy_string(to_modify); // TODO: do something better?
        where.replace(position_from, length, to_modify);
    }
}

void Fuzzier::modify_socket_struct(std::string& where, size_t position_from,
    size_t length)
{
    if (Random::struct_dist(Random::gen)) // switch the errno value with some P
    {
        if (length <= 0)
            return;

        std::string to_modify = where.substr(position_from, length);
        random.fuzzy_sockaddr_structure(to_modify);
        _fuzzied_structures[where.substr(position_from, length)] = to_modify;
        where.replace(position_from, length, to_modify);
    }
}

void Fuzzier::modify_return(SyscallSnapshot& snapshot_)
{
    switch (_cfg.fuzzy_approach) {
    case Fuzzy::returnvals:
        break;
    default:
        return;
    }
    if (Random::return_dist(Random::gen)) // switch the return value with some P
    {
        switch (snapshot_._returnLength) {
        case sizeof(int): {
            auto as_char = &snapshot_
                                ._inputs[snapshot_._inputs.size() - snapshot_._returnLength];
            /*
              Here, we need to be really causious, as we may to reffer to some memory
              size. Thus, we may choose either -1, or some value that is less than the
              current
              */
            int* as_int = reinterpret_cast<int*>(as_char);
            std::uniform_int_distribution<int> values_distribution(-1, *as_int);
            *as_int = values_distribution(Random::gen);
        } break;
        case sizeof(ssize_t): {
            auto as_char = &snapshot_
                                ._inputs[snapshot_._inputs.size() - snapshot_._returnLength];
            ssize_t* as_ssize_t = reinterpret_cast<ssize_t*>(as_char);
            /*
              Here, we need to be really causious, as we may to reffer to some memory
              size. Thus, we may choose either -1, or some value that is less than the
              current
              */
            std::uniform_int_distribution<ssize_t> values_distribution(-1, *as_ssize_t);
            *as_ssize_t = values_distribution(Random::gen);
        } break;
        }
    }
}
