#pragma once
#include <assert.h>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <pthread.h>
#include <string.h>
#include <string>
#include <sys/stat.h>
#include <unistd.h>
#include <vector>

/**
 * Class representing an snapshot of a syscall made by divine
 */
struct SyscallSnapshot {
    int _number;
    uint64_t _inode;
    std::string _inputs;
    int _errno;
    SyscallSnapshot* _next;
    SyscallSnapshot* _parent = nullptr; // optional
    size_t _timestamp;
    size_t _returnLength;
};

/**
 * @brief print prints basic info about given snapshot
 */
void print(std::vector<SyscallSnapshot>& snapshots);

/**
 * @brief getSyscall read full system call info (trace) into provided structure
 */
bool getSyscall(std::ifstream& istrm, SyscallSnapshot& snapshot);

/**
 * @brief serialize serialize snapshots back into a trace form readable by
 * divine
 */
void serialize(const std::vector<SyscallSnapshot>& snapshots, const std::string& output);

/**
 * @brief parse_input converts given input (trace) provided by divine
 * into a vector of spanshots of system calls
 */
std::vector<SyscallSnapshot> parse_input(const std::string& input, bool talk = false);
