#ifndef E2BIG
#define E2BIG 0
#endif
#ifndef EACCES
#define EACCES 0
#endif
#ifndef EADDRINUSE
#define EADDRINUSE 0
#endif
#ifndef EADDRNOTAVAIL
#define EADDRNOTAVAIL 0
#endif
#ifndef EAFNOSUPPORT
#define EAFNOSUPPORT 0
#endif
#ifndef EAGAIN
#define EAGAIN 0
#endif
#ifndef EALREADY
#define EALREADY 0
#endif
#ifndef EBADE
#define EBADE 0
#endif
#ifndef EBADF
#define EBADF 0
#endif
#ifndef EBADFD
#define EBADFD 0
#endif
#ifndef EBADMSG
#define EBADMSG 0
#endif
#ifndef EBADR
#define EBADR 0
#endif
#ifndef EBADRQC
#define EBADRQC 0
#endif
#ifndef EBADSLT
#define EBADSLT 0
#endif
#ifndef EBUSY
#define EBUSY 0
#endif
#ifndef ECANCELED
#define ECANCELED 0
#endif
#ifndef ECHILD
#define ECHILD 0
#endif
#ifndef ECHRNG
#define ECHRNG 0
#endif
#ifndef ECOMM
#define ECOMM 0
#endif
#ifndef ECONNABORTED
#define ECONNABORTED 0
#endif
#ifndef ECONNREFUSED
#define ECONNREFUSED 0
#endif
#ifndef ECONNRESET
#define ECONNRESET 0
#endif
#ifndef EDEADLK
#define EDEADLK 0
#endif
#ifndef EDEADLOCK
#define EDEADLOCK 0
#endif
#ifndef EDESTADDRREQ
#define EDESTADDRREQ 0
#endif
#ifndef EDOM
#define EDOM 0
#endif
#ifndef EDQUOT
#define EDQUOT 0
#endif
#ifndef EEXIST
#define EEXIST 0
#endif
#ifndef EFAULT
#define EFAULT 0
#endif
#ifndef EFBIG
#define EFBIG 0
#endif
#ifndef EHOSTDOWN
#define EHOSTDOWN 0
#endif
#ifndef EHOSTUNREACH
#define EHOSTUNREACH 0
#endif
#ifndef EHWPOISON
#define EHWPOISON 0
#endif
#ifndef EIDRM
#define EIDRM 0
#endif
#ifndef EILSEQ
#define EILSEQ 0
#endif
#ifndef EINPROGRESS
#define EINPROGRESS 0
#endif
#ifndef EINTR
#define EINTR 0
#endif
#ifndef EINVAL
#define EINVAL 0
#endif
#ifndef EIO
#define EIO 0
#endif
#ifndef EISCONN
#define EISCONN 0
#endif
#ifndef EISDIR
#define EISDIR 0
#endif
#ifndef EISNAM
#define EISNAM 0
#endif
#ifndef EKEYEXPIRED
#define EKEYEXPIRED 0
#endif
#ifndef EKEYREJECTED
#define EKEYREJECTED 0
#endif
#ifndef EKEYREVOKED
#define EKEYREVOKED 0
#endif
#ifndef EL2HLT
#define EL2HLT 0
#endif
#ifndef EL2NSYNC
#define EL2NSYNC 0
#endif
#ifndef EL3HLT
#define EL3HLT 0
#endif
#ifndef EL3RST
#define EL3RST 0
#endif
#ifndef ELIBACC
#define ELIBACC 0
#endif
#ifndef ELIBBAD
#define ELIBBAD 0
#endif
#ifndef ELIBMAX
#define ELIBMAX 0
#endif
#ifndef ELIBSCN
#define ELIBSCN 0
#endif
#ifndef ELIBEXEC
#define ELIBEXEC 0
#endif
#ifndef ELNRANGE
#define ELNRANGE 0
#endif
#ifndef ELOOP
#define ELOOP 0
#endif
#ifndef EMEDIUMTYPE
#define EMEDIUMTYPE 0
#endif
#ifndef EMFILE
#define EMFILE 0
#endif
#ifndef EMLINK
#define EMLINK 0
#endif
#ifndef EMSGSIZE
#define EMSGSIZE 0
#endif
#ifndef EMULTIHOP
#define EMULTIHOP 0
#endif
#ifndef ENAMETOOLONG
#define ENAMETOOLONG 0
#endif
#ifndef ENETDOWN
#define ENETDOWN 0
#endif
#ifndef ENETRESET
#define ENETRESET 0
#endif
#ifndef ENETUNREACH
#define ENETUNREACH 0
#endif
#ifndef ENFILE
#define ENFILE 0
#endif
#ifndef ENOANO
#define ENOANO 0
#endif
#ifndef ENOBUFS
#define ENOBUFS 0
#endif
#ifndef ENODATA
#define ENODATA 0
#endif
#ifndef ENODEV
#define ENODEV 0
#endif
#ifndef ENOENT
#define ENOENT 0
#endif
#ifndef ENOEXEC
#define ENOEXEC 0
#endif
#ifndef ENOKEY
#define ENOKEY 0
#endif
#ifndef ENOLCK
#define ENOLCK 0
#endif
#ifndef ENOLINK
#define ENOLINK 0
#endif
#ifndef ENOMEDIUM
#define ENOMEDIUM 0
#endif
#ifndef ENOMEM
#define ENOMEM 0
#endif
#ifndef ENOMSG
#define ENOMSG 0
#endif
#ifndef ENONET
#define ENONET 0
#endif
#ifndef ENOPKG
#define ENOPKG 0
#endif
#ifndef ENOPROTOOPT
#define ENOPROTOOPT 0
#endif
#ifndef ENOSPC
#define ENOSPC 0
#endif
#ifndef ENOSR
#define ENOSR 0
#endif
#ifndef ENOSTR
#define ENOSTR 0
#endif
#ifndef ENOSYS
#define ENOSYS 0
#endif
#ifndef ENOTBLK
#define ENOTBLK 0
#endif
#ifndef ENOTCONN
#define ENOTCONN 0
#endif
#ifndef ENOTDIR
#define ENOTDIR 0
#endif
#ifndef ENOTEMPTY
#define ENOTEMPTY 0
#endif
#ifndef ENOTRECOVERABLE
#define ENOTRECOVERABLE 0
#endif
#ifndef ENOTSOCK
#define ENOTSOCK 0
#endif
#ifndef ENOTSUP
#define ENOTSUP 0
#endif
#ifndef ENOTTY
#define ENOTTY 0
#endif
#ifndef ENOTUNIQ
#define ENOTUNIQ 0
#endif
#ifndef ENXIO
#define ENXIO 0
#endif
#ifndef EOPNOTSUPP
#define EOPNOTSUPP 0
#endif
#ifndef EOVERFLOW
#define EOVERFLOW 0
#endif
#ifndef EOWNERDEAD
#define EOWNERDEAD 0
#endif
#ifndef EPERM
#define EPERM 0
#endif
#ifndef EPFNOSUPPORT
#define EPFNOSUPPORT 0
#endif
#ifndef EPIPE
#define EPIPE 0
#endif
#ifndef EPROTO
#define EPROTO 0
#endif
#ifndef EPROTONOSUPPORT
#define EPROTONOSUPPORT 0
#endif
#ifndef EPROTOTYPE
#define EPROTOTYPE 0
#endif
#ifndef ERANGE
#define ERANGE 0
#endif
#ifndef EREMCHG
#define EREMCHG 0
#endif
#ifndef EREMOTE
#define EREMOTE 0
#endif
#ifndef EREMOTEIO
#define EREMOTEIO 0
#endif
#ifndef ERESTART
#define ERESTART 0
#endif
#ifndef ERFKILL
#define ERFKILL 0
#endif
#ifndef EROFS
#define EROFS 0
#endif
#ifndef ESHUTDOWN
#define ESHUTDOWN 0
#endif
#ifndef ESPIPE
#define ESPIPE 0
#endif
#ifndef ESOCKTNOSUPPORT
#define ESOCKTNOSUPPORT 0
#endif
#ifndef ESRCH
#define ESRCH 0
#endif
#ifndef ESTALE
#define ESTALE 0
#endif
#ifndef ESTRPIPE
#define ESTRPIPE 0
#endif
#ifndef ETIME
#define ETIME 0
#endif
#ifndef ETIMEDOUT
#define ETIMEDOUT 0
#endif
#ifndef ETOOMANYREFS
#define ETOOMANYREFS 0
#endif
#ifndef ETXTBSY
#define ETXTBSY 0
#endif
#ifndef EUCLEAN
#define EUCLEAN 0
#endif
#ifndef EUNATCH
#define EUNATCH 0
#endif
#ifndef EUSERS
#define EUSERS 0
#endif
#ifndef EWOULDBLOCK
#define EWOULDBLOCK 0
#endif
#ifndef EXDEV
#define EXDEV 0
#endif
#ifndef EXFULL
#define EXFULL 0
#endif
#ifndef EXFULL
#define EXFULL 0
#endif
