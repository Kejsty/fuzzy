#include "random.h"
#include "errors.h"
#include <climits> //WORD_BIT
#define UL_64_MAX 0xffffffffffffffffULL

unsigned long rand_just_one_bit(unsigned char which)
{
    if (which > WORD_BIT)
        which = WORD_BIT;

    assert(which > 0);
    return (1UL << (rand() % which));
}

static unsigned long first_max_n_bits(int amount)
{
    assert(amount > 0);
    unsigned int num = rand() % (amount);
    return (UL_64_MAX << num) >> num;
}

static unsigned long rand_random_n_bits(int amount)
{
    assert(amount > 0);
    unsigned int num = rand() % (amount);
    unsigned int i;
    unsigned long r = 0;

    for (i = 0; i < num && amount - i > 0; i++)
        r |= (1UL << (rand() % (amount - i)));

    return r;
}

static unsigned long limit_size(unsigned long value)
{
    /* limit the size */
    switch (rand() % 4) {
    case 0:
        return value &= 0x000000ffffffffffULL;
    case 1:
        return value &= 0x0000ffffffffffffULL;
    case 2:
        return value &= 0x00ffffffffffffffULL;
    default:
        return value;
    }
}

static unsigned long increase_msb_distribution(unsigned long value)
{
    /* increase distribution in MSB */
    unsigned int i;
    unsigned int rounds;

    rounds = rand() % 4;
    for (i = 0; i < rounds; i++)
        value |= (1UL << ((WORD_BIT - 1) - (rand() % 8)));
    return value;
}

/*
 * Pick 1 random byte, and repeat it through a long.
 */
static unsigned long repetite_byte(void)
{
    unsigned long r = RAND_BYTE();

    r = (r << 8) | r;
    r = (r << 16) | r;
#if WORD_BIT == 64
    r = (r << 32) | r;
#endif
    return r;
}

static void asci_representation_of_random(std::string& data)
{
    /* ascii representation of random numbers */
    static char separators[] = {
        ':',
        ',',
        '.',
        ' ',
        '-',
        '\0',
    };
    char separator = separators[rand() % sizeof(separators)];

    //    char *p = &data[0];

    size_t pos = 0;
    while (pos + 23 < data.length()) // Here 23 is the biggest value poosible
    // returned by some std::sprintf + separator
    {
        if (CHOOSE()) {
            /* hex */
            switch (rand() % 3) {
            case 0:
                pos += std::sprintf(&data[pos], "0x%lx",
                    static_cast<unsigned long>(rand_64()));
                break;
            case 1:
                pos += std::sprintf(&data[pos], "0x%lx",
                    static_cast<unsigned long>(rand_64()));
                break;
            case 2:
                pos += std::sprintf(&data[pos], "0x%x", static_cast<int>(rand_64()));
                break;
            }
        } else {
            /* decimal */

            switch (rand() % 3) {
            case 0:
                pos += std::sprintf(&data[pos], "%lu",
                    static_cast<unsigned long>(rand_64()));
                break;
            case 1:
                pos += std::sprintf(&data[pos], "%u",
                    static_cast<unsigned int>(rand_64()));
                break;
            case 2:
                pos += std::sprintf(&data[pos], "%u", static_cast<unsigned char>(rand()));
                break;
            }
        }

        if (CHOOSE()) {
            /* insert commas and/or spaces */
            *&data[pos] = separator;
            pos++;
        }
    }
}

/*
 * Generate and munge a 64bit number.
 */
uint64_t rand_64(void)
{
    uint64_t result = 0;

    switch (rand() % 7) {

    /* 8-bit ranges */
    case 0:
        result = RAND_BYTE();
        break;
    /* 33:64-bit ranges. */
    case 1:
        result = rand_just_one_bit(64);
        break;
    case 2:
        result = first_max_n_bits(32);
        break;
    case 3:
        result = rand_random_n_bits(32);
        break;
    case 4:
        result = static_cast<uint64_t>(rand()) << 32 | rand();
        break;
    case 5:
        result = repetite_byte();
        break;
    // small 64bit negative number.
    case 6:
        return 0 - ((rand() % 10) + 1);
    }

    result = limit_size(result);

    /* Once in 25 calls invert the number */
    if ((rand() % 25) & 1)
        result = ~result;

    /* increase distribution in MSB */
    if ((rand() % 10) & 1) {
        result = increase_msb_distribution(result);
    }

    /* Once in 25 calls */
    if ((rand() % 25) & 1)
        result = ~result + 1;

    return result;
}

std::random_device Random::rd;
std::mt19937 Random::gen(rd());
std::bernoulli_distribution Random::errno_dist(double(1) / 8);
std::bernoulli_distribution Random::return_dist(double(1) / 8);
std::bernoulli_distribution Random::struct_dist(double(1) / 16);
std::uniform_int_distribution<int> Random::errno_values_dist(error_min,
    error_max);

Random::Random(Fuzzy fuzzy_approach_)
    : _fuzzy_approach(fuzzy_approach_)
    , _data_dist(approach_to_distribution(fuzzy_approach_))
{
    if (_fuzzy_approach == Fuzzy::flip_bits)
        return;

    for (const auto& file : { "/proc/sysrq-trigger", "/proc/kmem", "/proc/kcore",
             "/dev/log", "/dev/mem", "/dev/kmsg", "/dev/kmem" }) {
        auto opened_file = std::make_unique<std::ifstream>(file);
        if (opened_file->good()) {
            streams.push_back(std::move(opened_file));
        }
    }
}

void Random::modify_with_original(std::string& data)
{
    switch (rand() % 4) {
    case 0:
        for (size_t i = 0; i < data.size();) {
            if (data.size() - i + 1 <= 0)
                return;
            size_t size = rand() % (data.size() - i + 1);
            std::string to_modify = data.substr(i, size);
            generate_rand_bytes(to_modify);
            data.replace(i, size, to_modify);
            i += size;

            if (data.size() - i + 1 <= 0)
                return;

            size = static_cast<size_t>(rand())  % (data.size() - i + 1);
            i += size; //skip some data
        }
        return;
    case 1: //sort data
        std::sort(data.begin(), data.end());
        return;
    case 2: //do some random permutation
        std::random_shuffle(data.begin(), data.end());
        return;
    case 3:
        for (char& character : data) {
            if (CHOOSE() && character > 0) {
                auto move_for = rand() % std::min(static_cast<int>(character), 255 - character);
                if (CHOOSE())
                    character += move_for;
                else
                    character -= move_for;
            }
        }
    }
}

void Random::modify_with_random(std::string& data)
{
    if (data.empty())
        return;

    //Select random substring from text and duplicate it
    size_t start = static_cast<size_t>(rand()) % data.size();
    if (data.size() - start - 1 <= 0)
        return;

    size_t length = static_cast<size_t>(rand()) % (data.size() - start - 1);

    if (length < 1)
        return;

    std::string replacement = data.substr(start, length);
    for (size_t i = 0; i < data.size(); i += length) {
        data.replace(i, length, replacement);
    }
}

void Random::flip_bits_only(std::string& data)
{
    for (size_t position = 0; position < data.length(); position++) {
        if (CHOOSE())
            data[position] = data[position] ^ 0xff;
    }
}

void Random::fuzzy_string(std::string& data)
{
    switch (_fuzzy_approach) {
    case Fuzzy::flip_bits:
        flip_bits_only(data);
        return;
    case Fuzzy::random_only:
        modify_with_random(data);
        return;
    case Fuzzy::string:
    case Fuzzy::returnvals:
        if (CHOOSE())
            modify_with_random(data);
        else
            modify_with_original(data);
        return;
    }
}

void Random::generate_rand_bytes(std::string& data)
{
    unsigned int randrange = data.length() < 24
        ? 6
        : 9; // asci_representation_of_random work only on longes strings
    switch (static_cast<uint>(rand())  % randrange) {
    case 0:
        /* Garbage. */
        {
            for (size_t position = 0; position < data.length(); position++) {
                if (CHOOSE())
                    data[position] = RAND_BYTE();
            }
        }
        break;
    case 1:
        std::fill(data.begin(), data.end(), 0);
        return;
    case 2:
        std::fill(data.begin(), data.end(), 0xff);
        return;
    case 3:
        std::fill(data.begin(), data.end(), RAND_BYTE());
        return;
    case 4: {
        for (size_t position = 0; position < data.length(); position++)
            if (CHOOSE())
                data[position] = static_cast<unsigned char>(CHOOSE());
    } break;
    case 5: {
        for (size_t position = 0; position < data.length(); position++)
            data[position] = data[position] + CHOOSE();
    } break;
    case 6: {
        if (!streams.empty()) {
            size_t selected = rand() % streams.size();
            streams[selected]->read(&data[0], data.size());
            if (!streams[selected]->good())
                streams.erase(streams.begin() + selected); // remove if no longer usable
        }
    }
        [[clang::fallthrough]];
    case 7:
    case 8: // make the P bigger
        asci_representation_of_random(data);
        break;
    }
}

void Random::fuzzy_sockaddr_structure(std::string& data)
{
    struct sockaddr* as_struct = reinterpret_cast<struct sockaddr*>(&data[0]);
    sa_family_t original_sa_family = as_struct->sa_family;
    generate_rand_bytes(data);
    as_struct->sa_family = original_sa_family;
}
