#pragma once

#include "../trace.h"
#include <cstring>
#include <tuple>
#include <type_traits>

using String = std::string;

namespace parsing {

bool memory(String& _inputs)
{
    size_t cut = sizeof(int);
    if (_inputs.size() < cut) {
        return false;
    }

    auto lengthData = _inputs.substr(0, cut);
    int length = *reinterpret_cast<const int*>(lengthData.data());
    _inputs = _inputs.substr(cut);

    if (length <= 0)
        return true;

    if (_inputs.size() < static_cast<size_t>(length))
        return false;

    _inputs = _inputs.substr(static_cast<size_t>(length));
    return true;
}

/**
 * @brief memory_size Gets the memory size of MEM in data. Used when there
 * is no interest in the data, only in its size
 * @param _inputs data, expects to start with the lenght of MEM structure
 * folowed by its data. Cuts the size info as well as the data itself.
 * @return the length of memory data read.
 */
uint memory_size(String& _inputs)
{
    size_t cut = sizeof(int);
    if (_inputs.size() < cut) {
        return 0;
    }

    auto lengthData = _inputs.substr(0, cut);
    int length = *reinterpret_cast<const int*>(lengthData.data());
    _inputs = _inputs.substr(cut);

    if (length <= 0)
        return 0;

    if (_inputs.size() < static_cast<size_t>(length))
        return 0;

    _inputs = _inputs.substr(static_cast<size_t>(length));
    return static_cast<uint>(length);
}

template <typename Type, typename = std::enable_if_t<std::is_pointer<Type>::value>>
uint structure_size(String& _inputs)
{
    size_t cut = sizeof(bool);
    if (_inputs.size() < cut) {
        return -1;
    }

    auto lengthData = _inputs.substr(0, cut);
    bool isNull = *reinterpret_cast<const bool*>(lengthData.data());
    _inputs = _inputs.substr(cut);

    if (isNull) {
        return 0;
    }

    size_t length = sizeof(std::remove_pointer<Type>);
    if (length <= 0)
        return 0;

    if (_inputs.size() < length)
        return 0;

    _inputs = _inputs.substr(static_cast<size_t>(length));
    return static_cast<uint>(length);
}

uint socket_structure_size(String& _inputs)
{
    size_t cut = sizeof(int);
    if (_inputs.size() < cut) {
        return 0;
    }

    auto lengthData = _inputs.substr(0, cut);
    int length = *reinterpret_cast<const int*>(lengthData.data());
    _inputs = _inputs.substr(cut);

    if (length <= 0)
        return 0;

    if (_inputs.size() < static_cast<size_t>(length)) {
        return 0;
    }

    _inputs = _inputs.substr(length);
    return static_cast<uint>(length);
}

template <typename Type,
    class NoPointer = std::remove_pointer<Type>,
    typename = std::enable_if_t<(!std::is_const<typename NoPointer::type>::value)>>
bool array(String& _inputs, Type process, int& count)
{
    size_t cut = sizeof(int);
    if (_inputs.size() < cut)
        return false;

    auto lengthData = _inputs.substr(0, cut);
    int length_info = *reinterpret_cast<const int*>(lengthData.data());
    _inputs = _inputs.substr(cut);

    if (length_info <= 0) {
        return true;
    }

    size_t length = static_cast<size_t>(length_info); // here I know it's ok

    if (_inputs.size() < length)
        return false;

    count = length_info;

    auto data = _inputs.substr(0, length);
    char* memory = reinterpret_cast<char*>(Type(process));
    std::memcpy(memory, data.data(), length);
    _inputs = _inputs.substr(length);
    return true;
}

/** if not interested in data only size*/
uint array_size(String& _inputs)
{
    size_t cut = sizeof(int);
    if (_inputs.size() < cut)
        return 0;

    auto lengthData = _inputs.substr(0, cut);
    int length = *reinterpret_cast<const int*>(lengthData.data());
    _inputs = _inputs.substr(cut);

    if (length <= 0) {
        return 0;
    }

    if (_inputs.size() < static_cast<size_t>(length))
        return 0;

    auto data = _inputs.substr(0, static_cast<size_t>(length));
    _inputs = _inputs.substr(static_cast<size_t>(length));
    return static_cast<uint>(length);
}

uint length(String& _inputs)
{
    size_t cut = sizeof(int);
    if (_inputs.size() < cut)
        return 0;

    auto lengthData = _inputs.substr(0, cut);
    int length = *reinterpret_cast<const int*>(lengthData.data());
    _inputs = _inputs.substr(cut);

    return length < 0 ? 0 : static_cast<uint>(length);
    ;
}

template <typename Type, typename = std::enable_if_t<std::is_pointer<Type>::value>>
bool structure(String& _inputs, Type process)
{
    size_t cut = sizeof(bool);
    if (_inputs.size() < cut) {
        return false;
    }

    auto lengthData = _inputs.substr(0, cut);

    bool isNull = *reinterpret_cast<const bool*>(lengthData.data());
    _inputs = _inputs.substr(cut);

    if (isNull) {
        process = NULL;
        return true;
    }

    size_t length = sizeof(std::remove_pointer<Type>);
    if (_inputs.size() < length)
        return false;

    auto data = _inputs.substr(0, length);
    char* structure = reinterpret_cast<char*>(Type(process));
    memcpy(structure, data.data(), length);
    _inputs = _inputs.substr(length);
    return true;
}

template <typename Type>
bool primitive(String& _inputs)
{
    size_t cut = sizeof(Type);
    if (_inputs.size() < cut) {
        return false;
    }
    _inputs = _inputs.substr(cut);
    return true;
}

template <typename Type>
bool return_value(String& _inputs, Type& type)
{
    size_t cut = sizeof(Type);
    if (_inputs.size() < cut) {
        return false;
    }
    type = *reinterpret_cast<const Type*>(_inputs.data());
    return true;
}

} // namespace parsing
