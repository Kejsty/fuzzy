#pragma once
#include <cassert>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <string>

enum Fuzzy {
    flip_bits,
    random_only,
    string,
    returnvals
};

enum Select {
    all,
    bigegst_coverage,
    logest_branch,
    fanout
};

struct Config {
    Config()
        : start(std::chrono::steady_clock::now())
    {
    }
    using timepoint = std::chrono::steady_clock::time_point;
    std::string path_to_divine{ "divine" };
    std::string program{ "test.cpp" };
    size_t fuzzy_per_round{ 10 };
    size_t taken_to_next{ 3 };
    std::string mode_of_execution{ "" };
    bool quiet{ false };
    std::string output_file{ "captures.zip" };
    Fuzzy fuzzy_approach{ returnvals };
    Select selection_approach{ bigegst_coverage };
    timepoint start;
    std::chrono::seconds time_to_run{ 0 };
};

inline Select string_to_select(const std::string& from)
{
    if (from == "all")
        return Select::all;
    if (from == "coverage")
        return Select::bigegst_coverage;
    if (from == "longest")
        return Select::logest_branch;
    if (from == "fanout")
        return Select::fanout;

    std::cerr << "Unsupported value for select approach: " << from << std::endl;
    assert(0);
}

inline Fuzzy string_to_fuzzy(const std::string& from)
{
    if (from == "bits")
        return Fuzzy::flip_bits;
    if (from == "random")
        return Fuzzy::random_only;
    if (from == "string")
        return Fuzzy::string;
    if (from == "return")
        return Fuzzy::returnvals;
    //todo: should not happed!
    std::cerr << "Unsupported value for fuzzy approach: " << from << std::endl;
    assert(0);
}
