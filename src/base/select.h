#pragma once
#include "../graph.h"
#include "config.h"
#include <cstdlib>
#include <istream>
#include <memory>
#include <string>
#include <vector>

class Selector {
    using graph_pair = std::pair<std::string, std::unique_ptr<divineGraph>>;

public:
    Selector(Config config_);
    std::vector<std::string> select(std::vector<graph_pair> graphs_);

    std::ostream& me(std::ostream& out_ = std::cout)
    {
        out_ << "[Selector]:";
        return out_;
    }

private:
    std::vector<std::string> select_best_coverage(std::vector<graph_pair> graphs_);
    std::vector<std::string> select_all(std::vector<graph_pair> graphs_);
    std::vector<std::string> select_longest_path(std::vector<graph_pair> graphs_);
    std::vector<std::string> select_best_fanout(std::vector<graph_pair> graphs_);

    Config _config;
    size_t last_coverage{ 0 };
    size_t coverage_not_increased{ 0 };
};
