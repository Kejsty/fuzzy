#include "select.h"

Selector::Selector(Config config_)
    : _config(config_)
{
}

std::vector<std::string> Selector::select(std::vector<graph_pair> graphs_)
{
    switch (_config.selection_approach) {
    case Select::all:
        return select_all(std::move(graphs_));
    case Select::bigegst_coverage:
        return select_best_coverage(std::move(graphs_));
    case Select::fanout:
        return select_best_fanout(std::move(graphs_));
    case Select::logest_branch:
        return select_longest_path(std::move(graphs_));
    }
    return select_all(std::move(graphs_));
}

std::vector<std::string> Selector::select_all(std::vector<graph_pair> graphs_)
{
    std::vector<std::string> results;
    for (const auto& pair : graphs_) {
        results.push_back(pair.first);
    }
    return results;
}

std::vector<std::string>
Selector::select_best_coverage(std::vector<graph_pair> graphs_)
{
    using trie_pair = std::pair<std::string, std::set<std::string>>;
    if (graphs_.empty()) {
        return {};
    }

    if (graphs_.size() <= _config.taken_to_next) {
        me() << "Number of graphs for select is too big, will take all graphs";
        return select_all(std::move(graphs_));
    }

    std::vector<trie_pair> traces;

    for (const auto& pair : graphs_) {
        traces.push_back({ pair.first, pair.second->getAllTraces() });
    }

    me() << "Getting " << _config.taken_to_next << " graphs with the biggest coverage\n";

    // Two graphs containing with the biggest coverage
    std::vector<std::string> results;
    size_t next_coverage = 0;
    auto compare_coverage = [this](size_t next_coverage) {
        if (next_coverage > last_coverage) {
            coverage_not_increased = 0;
            last_coverage = next_coverage;
        } else {
            ++coverage_not_increased;
        }
    };

    for (size_t i = 0; i < _config.taken_to_next; ++i) {
        auto biggest = std::max_element(
            traces.begin(), traces.end(), [](trie_pair& fst, trie_pair& snd) {
                return fst.second.size() < snd.second.size();
            });

        if (biggest == traces.end() || biggest->second.size() == 0) {
            me() << i + 1 << " Graphs already covered all curretnly possible paths, skipping to next";
            compare_coverage(next_coverage);
            return results;
        }

        next_coverage += biggest->second.size();
        results.push_back(biggest->first);

        for (auto& pair : traces) {
            std::set<std::string> difference;
            std::set_difference(pair.second.begin(), pair.second.end(), biggest->second.begin(), biggest->second.end(), std::inserter(difference, difference.begin()));
            pair.second = std::move(difference);
        }
        assert(biggest->second.size() == 0);
    }
    compare_coverage(next_coverage);
    return results;
}

std::vector<std::string>
Selector::select_longest_path(std::vector<graph_pair> graphs_)
{
    if (graphs_.empty()) {
        me() << "No graphs to select!\n";
        return {};
    }

    if (graphs_.size() <= _config.taken_to_next) {
        me() << "Number of graphs to select is too big,taking them all";
        return select_all(std::move(graphs_));
    }

    me() << "Getting " << _config.taken_to_next << " graphs with the longest path\n";
    std::vector<std::string> results;

    for (size_t i = 0; i < _config.taken_to_next; ++i) {
        auto biggest = std::max_element(
            graphs_.begin(), graphs_.end(), [](graph_pair& fst, graph_pair& snd) {
                return fst.second->get_max_depth() < snd.second->get_max_depth();
            });

        results.push_back(biggest->first);
        graphs_.erase(biggest);
    }
    return results;
}

std::vector<std::string>
Selector::select_best_fanout(std::vector<graph_pair> graphs_)
{
    if (graphs_.empty()) {
        me() << "No graphs to select!\n";
        return {};
    }

    if (graphs_.size() <= _config.taken_to_next) {
        me() << "Number of graphs to select is too big,taking them all";
        return select_all(std::move(graphs_));
    }

    std::vector<std::string> results;
    me() << "Getting " << _config.taken_to_next << " graphs with the biggest fanout\n";

    for (size_t i = 0; i < _config.taken_to_next; ++i) {
        auto biggest = std::max_element(
            graphs_.begin(), graphs_.end(),
            [](const graph_pair& fst, const graph_pair& snd) {
                return fst.second->get_max_fanout() < snd.second->get_max_fanout();
            });

        results.push_back(biggest->first);
        graphs_.erase(biggest);
    }
    return results;
}
