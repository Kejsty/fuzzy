#pragma once
#include <cstdlib>
#include <fstream>
#include <istream>
#include <memory>
#include <random>
#include <string>
#include <vector>

#define CHOOSE() (rand() & 1)
#define RAND_BYTE() (rand() & 0xff)

static size_t divide_constant = 1;

#include "config.h"
#include <netinet/in.h>
uint64_t rand_64(void);

inline double approach_to_distribution(Fuzzy approach)
{
    double one = 1;
    return one / (4 * divide_constant);
}

class Random {
public:
    Random(Fuzzy fuzzy_approach_);

    void generate_rand_bytes(std::string& data);
    void fuzzy_string(std::string& data);
    void fuzzy_sockaddr_structure(std::string& data);

private:
    std::vector<std::unique_ptr<std::ifstream>> streams;
    Fuzzy _fuzzy_approach;

    void modify_with_original(std::string& data);
    void modify_with_random(std::string& data);
    void flip_bits_only(std::string& data);

public:
    static std::random_device rd;
    static std::mt19937 gen;
    static std::bernoulli_distribution errno_dist;
    static std::bernoulli_distribution return_dist;
    static std::bernoulli_distribution struct_dist;
    static std::uniform_int_distribution<int> errno_values_dist;
    std::bernoulli_distribution _data_dist;
};
