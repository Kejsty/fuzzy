#include "launcher.h"
#include <algorithm>

namespace commands {

static std::string program{ " passthruTest/differentInodes.cpp " };
static std::string output_filename{ "passthrough.out" };

static std::string verify{ " verify " };
static std::string draw{ " draw --distance 100000 " };
static std::string passthrough{ " exec " };
static std::string base{ " -o config:fuzzy -o syssource:" };
static std::string be_quiet{ " &> /dev/null " };
static std::string redirect_compilation{ " 2> fuzzy_working_dir/compilation " };
static std::string redirect_run{ " > fuzzy_working_dir/run " };
static std::string report_to_file{ " --report-filename " };
static std::string malloc_nofail{ " -o nofail:malloc " };

std::string render_as(const std::string& name)
{
    return "--render \"tee " + name + "\"";
}
} // namespace commands

void Launcher::start()
{
    me() << "Starting\n";
    _state = Starting;
    me() << "Preparing environment\n";

    int dir_err = mkdir("fuzzy_working_dir", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (dir_err == -1) {
        std::cerr
            << "Unable to create directory  fuzzy_working_dir, rewritting old \n";
    }

    std::string command = "cp errors fuzzy_working_dir/ &> /dev/null";
    int returnval = system(command.c_str());

    if (returnval < 0) {
        _state = Error;
        me(std::cerr) << "Command: " << command << "unsuccesfull \n";
    }

    me() << "launching first passthrough mode, to generate passthrough.out \n";
    _state = Verify;

    auto mode = _config.mode_of_execution.length()
        ? ("B=" + _config.mode_of_execution + " .")
        : ".";

    command = mode + _config.path_to_divine + commands::passthrough + commands::malloc_nofail + _config.program + commands::redirect_compilation + commands::redirect_run;
    me() << "Running " << command << std::endl;
    returnval = system(command.c_str());

    if (returnval < 0) {
        _state = Error;
        me(std::cerr);
        std::cerr << "Unable to create child\n";
        return;
    }

    if (!validate_first_run()) {
        return;
    }

    process_passthrough_output("passthrough.out");
}

std::string Launcher::include_passtrough()
{
    std::string command = "mv passthrough.out fuzzy_working_dir/original &> /dev/null";
    int returnval = system(command.c_str());

    if (returnval < 0) {
        _state = Error;
        me(std::cerr) << "Command: " << command << "unsuccesfull \n";
        return {};
    }

    return "fuzzy_working_dir/original";
}

void Launcher::process_passthrough_output(std::string passthrought_file_)
{
    _state = Fuzzy;
    me() << "Fuzzing passthrough.out\n";
    // Contains new files with fuzzied passthrough.out file (on the right spots)
    auto names = _fuzzier.find_modifiable_parts(passthrought_file_);

    if (names.empty()) {
        _state = Error;
        me(std::cerr) << "No system calls to fuzzy\n";
        return;
    }

    me() << "Adding also original passthrough file\n";
    names.push_back(include_passtrough());

    for (;; ++_iteration) {
        if (have_time_elapsed()) {
            me() << "Time has elapsed, finishing\n";
            break;
        }
        me() << "Starting next iteration\n";
        auto selected = make_round(names);
        if (selected.empty()) {
            _state = Error;
            me(std::cerr) << "Can't continue, no file to work with\n";
            break;
        }
        names = selected; // keep only the best for next found
        // Provide new names
        me() << "Finding next modifiable parts:\n";
        for (const auto& name : selected) {
            auto next = _fuzzier.find_modifiable_parts(name);
            std::copy(next.begin(), next.end(), std::back_inserter(names));
        }
    }
    _state = Finishing;
    zip_result();
    print_end_info();
}

void Launcher::zip_result()
{

    std::string command = "zip -r " + _config.output_file + " " + "fuzzy_working_dir/ &> /dev/null";
    int returnval = system(command.c_str());

    if (returnval < 0) {
        _state = Error;
        me(std::cerr) << "Command: " << command << "unsuccesfull \n";
        return;
    } else
        me() << "Captured files saved into " << _config.output_file << std::endl;

    command = "rm -rf fuzzy_working_dir/ &> /dev/null";
    returnval = system(command.c_str());

    if (returnval < 0) {
        _state = Error;
        me(std::cerr) << "Command: " << command << "unsuccesfull \n";
        return;
    }
    return;
}

void Launcher::print_end_info()
{
    using namespace std::chrono;
    duration<double> runned_for = duration_cast<duration<double>>(steady_clock::now() - _config.start);

    me() << "Finishing fuzzer run. Some statistics:\n"
         << "\t runned for: " << runned_for.count() << " seconds, "
         << "number of Errornous traces found: " << errornous_traces.size()
         << std::endl;
    me() << "All inputs for subsequent re-run can be found in "
         << _config.output_file << std::endl;

    if (!errornous_traces.empty()) {
        std::cout << "To reproduce errors call: \n";
        for (const auto& file : errornous_traces) {
            auto mode = _config.mode_of_execution.length()
                ? ("B=" + _config.mode_of_execution + " .")
                : ".";
            auto command = mode + _config.path_to_divine + commands::verify + commands::malloc_nofail + " -o config:fuzzy -o syssource:" + file + " " + _config.program;
            std::cout << "\t" << command << std::endl;
        }
    }
}

bool Launcher::validate_first_run()
{
    std::ifstream compilation("fuzzy_working_dir/compilation");

    if (!compilation) {
        me(std::cerr) << "Compilation info not present!\n";
        return false;
    }

    std::string line;
    while (!compilation.eof()) {
        std::getline(compilation, line);
        if ((line.find("E: validator error: ") != std::string::npos) || (line.find("ERROR:") != std::string::npos)) {
            _state = Error;
            me(std::cerr) << "Compilation error: " << line << std::endl;
            return false;
        }
    }

    std::ifstream run("fuzzy_working_dir/run");
    if (!run) {
        me(std::cerr) << "Run info not present!\n";
        return false;
    }

    while (!run.eof()) {
        std::getline(run, line);

        if (line.find("FATAL:") != std::string::npos && line.find("error in userspace") != std::string::npos) {
            me(std::cerr) << "Run failed in userspace, it's useless to fuzzy file with error\n";
            return false;
        }
    }
    return true;
}

int Launcher::run_draw_session(const std::string& id, std::string input)
{
    auto mode = _config.mode_of_execution.length()
        ? ("B=" + _config.mode_of_execution + " .")
        : ".";

    auto command = mode + _config.path_to_divine + commands::draw + commands::malloc_nofail + commands::render_as(id) + commands::base + input + " " + _config.program + commands::be_quiet;
    int returnval = system(command.c_str());

    if (returnval < 0) {
        me(std::cerr) << "Failed to call: " << command << std::endl;
    }
    return returnval;
}

std::vector<std::string>
Launcher::make_round(std::vector<std::string> fuzzied_files_)
{
    std::vector<graph_pair> graphs;

    for (size_t i = 0; i < fuzzied_files_.size(); i += 4) { //run only 4 draws at once
        std::vector<std::future<int>> results;
        for (auto round = 0; round < 4 && i + round < fuzzied_files_.size(); ++round) {
            auto draw_name{ fuzzied_files_[i + round] + "_graph" };
            results.push_back(
                std::async(&Launcher::run_draw_session, this, draw_name, fuzzied_files_[i + round]));
        }

        for (auto round = 0; round < 4 && i + round < fuzzied_files_.size(); ++round) {
            int result = results[round].get();
            // wait for finish
            if (result == 0) {
                auto graphName{ fuzzied_files_[i + round] + "_graph" };
                graphs.push_back(
                    { fuzzied_files_[i + round], std::make_unique<divineGraph>(graphName) });
            }
        }
    }

    check_graphs(graphs);
    return _selector.select(std::move(graphs));
}

void Launcher::check_graphs(const std::vector<graph_pair>& graphs_)
{
    for (const auto& graph : graphs_) {
        if (graph.second->contains_error_trace())
            errornous_traces.insert(graph.first);
    }
}

bool Launcher::have_time_elapsed()
{
    using namespace std::chrono;
    if (_config.time_to_run == std::chrono::minutes(0))
        return false; //no time-limited

    duration<double> runned_for = duration_cast<duration<double>>(steady_clock::now() - _config.start);
    std::chrono::seconds in_seconds = duration_cast<std::chrono::seconds>(runned_for);
    return in_seconds > _config.time_to_run;
}
