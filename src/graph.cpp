#include "graph.h"
#include <algorithm>
#include <iostream>
#include <cstring>

enum Operation { Successor,
    Declaration,
    Unknown };

Operation parseOperation(char operation[4])
{
    if (strstr(operation, "->"))
        return Operation::Successor;
    else if (strstr(operation, "["))
        return Operation::Declaration;
    return Operation::Unknown;
}

divineGraph::divineGraph(const std::string& input)
{
    std::ifstream dot_graph(input);

    if (!dot_graph.is_open() || dot_graph.eof()) {
        std::cerr << "No such file or empty graph!";
        _ok = false;
        return;
    }

    while (true) {
        int node_id;
        dot_graph >> node_id;

        if (dot_graph.eof())
            return;
        else if (dot_graph.fail() || dot_graph.bad()) {
            // skip line
            dot_graph.clear();
            dot_graph.ignore(std::numeric_limits<std::streamsize>::max(),
                dot_graph.widen('\n'));
        } else {
            char operation[4];
            dot_graph.get(operation, 4);

            switch (parseOperation(operation)) {
            case Operation::Successor:
                if (!handle_oprration_successor(dot_graph, node_id))
                    return;
                break;
            case Operation::Declaration:
                max_node = std::max(max_node, node_id);
                nodes.insert({ node_id, Node(node_id) });
                dot_graph.ignore(std::numeric_limits<std::streamsize>::max(),
                    dot_graph.widen('\n'));
                break;
            case Operation::Unknown:
                _ok = false;
                return;
            }
        }
    }
}

void divineGraph::print() const
{
    for (size_t i = 0; i < nodes.size(); ++i) {
        auto node = find_node(i);
        if (node) {
            std::cout << "Node: ID: " << i << "successors: [";
            for (auto succ : node->succ)
                std::cout << succ << " ";
            std::cout << "]" << std::endl;
        } else
            std::cout << "Node: ID: " << i << " not found!";
    }
}

std::set<std::string> divineGraph::getAllTraces()
{
    for (int order = max_node; order > 0; --order) {
        auto* node = find_node(order);
        if (!node)
            continue;

        for (auto& some_pair : node->traces) {
            if (some_pair.first == node->id)
                continue; //omit myself

            auto* succ = find_node(some_pair.first);

            if (!succ) {
                if (!some_pair.second.empty()) {
                    node->info.traces.insert(some_pair.second);
                }
            } else {
                const auto& traces_o = succ->info.traces;
                if (traces_o.empty()) { //create
                    if (!some_pair.second.empty()) {
                        node->info.traces.insert(some_pair.second);
                    }
                } else { //attach
                    for (auto trace_o : traces_o) {
                        if (!some_pair.second.empty()) // dont push empty traces
                        {
                            trace_o = some_pair.second + trace_o;
                        }
                        if (!trace_o.empty())
                            node->info.traces.insert(std::move(trace_o));
                    }
                }
            }
        }
    }
    return find_node(1)->info.traces;
}

size_t divineGraph::get_max_depth()
{
    auto* start = find_node(1);
    assert(start);

    if (start->info.depth != 0)
        return start->info.depth;

    max_depth();
    return start->info.depth;
}

size_t divineGraph::get_max_fanout()
{
    auto* start = find_node(1);
    assert(start);

    if (start->info.max_fanout != 0)
        return start->info.max_fanout;

    max_fanout();
    return start->info.max_fanout;
}

void divineGraph::max_depth()
{

    for (int order = max_node; order > 0; --order) {
        auto* node = find_node(order);
        if (!node)
            continue;

        size_t max_chid_depth = 0;
        for (auto child_id : node->succ) {
            auto* child = find_node(child_id);
            if (child)
                max_chid_depth = std::max(max_chid_depth, child->info.depth);
        }
        max_chid_depth += 1; // for me
        node->info.depth = max_chid_depth;
    }
}

void divineGraph::max_fanout()
{
    for (int order = max_node; order > 0; --order) {
        auto* node = find_node(order);
        if (!node)
            continue;

        size_t node_max_fanout = node->succ.size();
        for (auto child_id : node->succ) {
            auto* child = find_node(child_id);
            if (child)
                node_max_fanout = std::max(node_max_fanout, child->info.max_fanout);
        }
        node->info.max_fanout = node_max_fanout;
    }
}

//--------- PARSING -------------------------------------------------------------------------------
void divineGraph::process_info(std::string info, Node& about, uint32_t succ_)
{
    auto prev = 0;
    auto pos = info.find("\\l");
    std::string all_info;
    while (pos != std::string::npos) {
        auto full_info = info.substr(prev, pos - prev);
        if (full_info.find("fuzz_info: Could not pass a system call") != std::string::npos) // it was a passnot info
            break;
        auto current_info = shorten_info(std::move(full_info));
        _error_found = _error_found | current_info.second;
        if (current_info.first != 0)
            all_info += std::to_string(current_info.first) + ",";
        prev = pos;
        pos = info.find("\\l", pos + 2);
    }
    about.traces.push_back({ succ_, all_info });
}

bool divineGraph::handle_oprration_successor(std::ifstream& dot_graph,
    int node_id)
{
    int succ_id;

    dot_graph >> succ_id;
    if (dot_graph.fail()) {
        std::cerr << "Succerssor not provided!\n";
        _ok = false;
        return false;
    }

    auto parent = nodes.find(node_id);
    if (parent == nodes.end()) {
        std::cerr << "Parent (" << node_id << ") for " << succ_id
                  << " not found!\n";
        _ok = false;
        return false;
    }

    parent->second.succ.push_back(succ_id);
    std::string input;
    getline(dot_graph, input);
    process_info(std::move(input), parent->second, succ_id);
    return true;
}

std::pair<size_t, bool> divineGraph::shorten_info(std::string from)
{
    size_t mask_result = 0;
    bool error_found = false;

    constexpr size_t sysnot = static_cast<size_t>(1) << 21; //10
    constexpr size_t passed = static_cast<size_t>(1) << 20; //01
    constexpr size_t fatal = static_cast<size_t>(3) << 20; //11

    auto id_start = from.find_first_of("0123456789");
    while (id_start != std::string::npos) {
        std::string thread_id;
        thread_id += from[id_start]; //thread id
        ++id_start;

        if (id_start >= from.size() || !std::isdigit(from[id_start])) {
            mask_result |= (std::stoi(thread_id) << 10);
            break;
        }
    }

    size_t pos = 0;
    if ((pos = from.find("SYSNOT")) != std::string::npos) {
        auto syscall_start = from.find_first_of("0123456789", pos);
        mask_result |= sysnot;
        std::string syscallnum;
        while (id_start != std::string::npos) {
            syscallnum += from[syscall_start];
            ++syscall_start;
            if (syscall_start >= from.size() || !std::isdigit(from[syscall_start])) {
                mask_result |= std::stoi(syscallnum);
                break;
            }
        }
    } else if ((pos = from.find("YES")) != std::string::npos) {
        auto syscall_start = from.find_first_of("0123456789", pos);
        mask_result |= passed;
        std::string syscallnum;
        while (id_start != std::string::npos) {
            syscallnum += from[syscall_start];
            ++syscall_start;
            if (syscall_start >= from.size() || !std::isdigit(from[syscall_start])) {
                mask_result |= std::stoi(syscallnum);
                break;
            }
        }
    } else if ((pos = from.find("FATAL")) != std::string::npos) {
        mask_result |= fatal;
        error_found = true;
    } else
        mask_result = 0;
    return { mask_result, error_found };
}
