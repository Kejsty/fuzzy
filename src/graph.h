#pragma once

#include <algorithm>
#include <cassert>
#include <fstream>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

class divineGraph {
    using Id_t = size_t;
    using Seq = std::vector<Id_t>;

    class Node {
        struct Info {
            size_t depth = 0;
            size_t max_fanout = 0;
            std::set<std::string> traces;
        };

    public:
        const uint id;
        Node(uint id)
            : id(id)
        {
        }
        std::vector<uint32_t> succ;
        std::vector<std::pair<uint32_t, std::string>> traces;
        Info info;
    };

public:
    divineGraph(const std::string& input);

    void print() const;
    bool is_ok() const
    {
        return _ok;
    }
    bool contains_error_trace() const
    {
        return _error_found;
    }

    const Node* find_node(int number) const
    {
        auto child = nodes.find(number);
        return (child == nodes.end() ? nullptr : &(child->second));
    }

    //Properties
    std::set<std::string> getAllTraces();
    size_t get_max_depth();
    size_t get_max_fanout();

private:
    Node* find_node(int number)
    {
        auto child = nodes.find(number);
        return (child == nodes.end() ? nullptr : &(child->second));
    }

    /**
   * @brief shorten_info a way to convert long message
   * provided for some edge in Divine graph
   * to some shorter form (to fasten impl.)
   * @param from the original long leading edge info
   * @return shortened version (or empty, in case the message does not suit to
   * any shortcut) for example, "FAULT" is being ignored as "FATAL" always
   * follows
   */
    static std::pair<size_t, bool> shorten_info(std::string from);

    void process_info(std::string info, Node& about, uint32_t succ_);
    bool handle_oprration_successor(std::ifstream& dot_graph, int node_id);
    void max_depth();
    void max_fanout();

private:
    std::unordered_map<uint, Node> nodes;
    int max_node{ 0 };
    friend Node;
    bool _ok{ true };
    bool _error_found{ false };
};
